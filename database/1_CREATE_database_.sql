CREATE DATABASE HungerKiller;

USE HungerKiller;

CREATE TABLE IF NOT EXISTS hk_addresses
(
    id      INTEGER AUTO_INCREMENT PRIMARY KEY,
    street  VARCHAR(50) NOT NULL,
    town    VARCHAR(50) NOT NULL,
    zipcode VARCHAR(6)
);

CREATE TABLE IF NOT EXISTS hk_restaurants
(
    id           INTEGER AUTO_INCREMENT PRIMARY KEY,
    name         VARCHAR(50) NOT NULL,
    description  VARCHAR(200),
    open_from    TIME,
    open_till    TIME,
    address_id   INTEGER     NOT NULL,
    phone_number VARCHAR(20),

    FOREIGN KEY hk_restaurant_address_fk (address_id) REFERENCES hk_addresses (id)
);

CREATE TABLE IF NOT EXISTS hk_dishes
(
    id            INTEGER AUTO_INCREMENT PRIMARY KEY,
    name          VARCHAR(50) NOT NULL,
    description   VARCHAR(200),
    price         NUMERIC(10, 2),
    restaurant_id INTEGER     NOT NULL,

    FOREIGN KEY hk_dish_restaurant_fk (restaurant_id) REFERENCES hk_restaurants (id)
);

CREATE TABLE IF NOT EXISTS hk_users
(
    id                  INTEGER AUTO_INCREMENT PRIMARY KEY,
    name                VARCHAR(25),
    surname             VARCHAR(50),
    email               VARCHAR(50) UNIQUE NOT NULL,
    password            VARCHAR(100)       NOT NULL,
    phone_number        VARCHAR(15),
    delivery_address_id INTEGER            NOT NULL,
    role                VARCHAR(20)        NOT NULL,

    FOREIGN KEY hk_user_address_fk (delivery_address_id) REFERENCES hk_addresses (id)
);

CREATE TABLE IF NOT EXISTS hk_orders
(
    id            INTEGER AUTO_INCREMENT PRIMARY KEY,
    restaurant_id INTEGER     NOT NULL,
    user_id       INTEGER     NOT NULL,
    creation_time DATE,
    status        VARCHAR(15) NOT NULL,

    FOREIGN KEY hk_order_restaurant_fk (restaurant_id) REFERENCES hk_restaurants (id),
    FOREIGN KEY hk_order_user_fk (user_id) REFERENCES hk_users (id)
);

CREATE TABLE IF NOT EXISTS hk_order_dishes
(
    order_id INTEGER,
    dish_id  INTEGER,

    FOREIGN KEY hk_order_dishes_order_fk (order_id) REFERENCES hk_orders (id),
    FOREIGN KEY hk_order_dishes_dish_fk (dish_id) REFERENCES hk_dishes (id)
);

CREATE TABLE hk_deliverers
(
    id              INTEGER AUTO_INCREMENT PRIMARY KEY,
    service_in_city VARCHAR(50) NOT NULL
);

CREATE TABLE hk_deliverer_schedule
(
    id           INTEGER AUTO_INCREMENT PRIMARY KEY,
    deliverer_id INTEGER     NOT NULL,
    day_of_week  VARCHAR(10) NOT NULL,
    time_start   TIME        NOT NULL,
    time_end     TIME        NOT NULL,
    FOREIGN KEY hk_deliverer_schedule_deliverer (deliverer_id) REFERENCES hk_deliverers (id)
);
