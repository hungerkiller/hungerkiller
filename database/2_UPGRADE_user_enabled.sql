ALTER TABLE HungerKiller.hk_users
    ADD COLUMN enabled CHAR(1) CHECK (enabled IN ('Y', 'N')) NOT NULL