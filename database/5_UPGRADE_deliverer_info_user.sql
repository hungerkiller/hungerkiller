ALTER TABLE HungerKiller.hk_deliverers
    ADD user_id INTEGER,
    ADD CONSTRAINT FOREIGN KEY (user_id) REFERENCES HungerKiller.hk_users (id);