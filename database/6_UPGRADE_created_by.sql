ALTER TABLE HungerKiller.hk_restaurants
    ADD COLUMN owner_id INTEGER;

ALTER TABLE HungerKiller.hk_restaurants
    ADD CONSTRAINT hk_restaurant_restaurant_owner_fk
        FOREIGN KEY (owner_id) REFERENCES HungerKiller.hk_users (id)
            ON DELETE SET NULL;


ALTER TABLE HungerKiller.hk_dishes
    ADD COLUMN owner_id INTEGER;

ALTER TABLE HungerKiller.hk_restaurants
    ADD CONSTRAINT hk_dishes_dish_owner_fk
        FOREIGN KEY (owner_id) REFERENCES HungerKiller.hk_users (id)
            ON DELETE SET NULL;;