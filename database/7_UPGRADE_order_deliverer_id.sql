ALTER TABLE HungerKiller.hk_orders
    ADD deliverer_id INTEGER,
    ADD CONSTRAINT FOREIGN KEY(deliverer_id) REFERENCES HungerKiller.hk_deliverers(id);