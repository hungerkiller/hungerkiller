ALTER TABLE hk_deliverers
    DROP FOREIGN KEY hk_deliverers_ibfk_1;

ALTER TABLE hk_deliverers
    ADD CONSTRAINT hk_deliverers_ibfk_1
        FOREIGN KEY (user_id) REFERENCES hk_users (id)
            ON DELETE CASCADE;
