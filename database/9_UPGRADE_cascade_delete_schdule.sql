ALTER TABLE hk_deliverer_schedule
    DROP FOREIGN KEY hk_deliverer_schedule_ibfk_1;

ALTER TABLE hk_deliverer_schedule
    ADD CONSTRAINT hk_deliverer_schedule_ibfk_1
        FOREIGN KEY (deliverer_id) REFERENCES hk_deliverers (id)
            ON DELETE CASCADE;
