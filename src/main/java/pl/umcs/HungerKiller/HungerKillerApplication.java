package pl.umcs.HungerKiller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HungerKillerApplication {

    public static void main(String[] args)
    {
        SpringApplication.run(HungerKillerApplication.class, args);
    }

}
