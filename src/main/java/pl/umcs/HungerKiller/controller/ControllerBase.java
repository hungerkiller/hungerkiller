package pl.umcs.HungerKiller.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pl.umcs.HungerKiller.exceptions.BodyHandlingException;
import pl.umcs.HungerKiller.exceptions.NotOwnerException;
import pl.umcs.HungerKiller.exceptions.WrongUserRoleException;

public class ControllerBase {

    @ExceptionHandler(BodyHandlingException.class)
    private ResponseEntity<?> handleBodyHandlingException(BodyHandlingException exception)
    {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotOwnerException.class)
    private ResponseEntity<?> handleNotOwnerException(NotOwnerException exception)
    {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(WrongUserRoleException.class)
    private ResponseEntity<?> handleNotOwnerException(WrongUserRoleException exception)
    {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.FORBIDDEN);
    }
}
