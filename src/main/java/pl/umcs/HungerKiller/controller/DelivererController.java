package pl.umcs.HungerKiller.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.umcs.HungerKiller.openapi.api.DelivererApi;
import pl.umcs.HungerKiller.openapi.api.DelivererApiDelegate;
import pl.umcs.HungerKiller.service.DelivererService;

@RestController
@RequestMapping(value = "/hungerkiller/v1")
public class DelivererController extends ControllerBase implements DelivererApi {
    private final DelivererService delivererService;

    @Autowired
    public DelivererController(DelivererService delivererService)
    {
        this.delivererService = delivererService;
    }

    @Override
    public DelivererApiDelegate getDelegate()
    {
        return delivererService;
    }
}
