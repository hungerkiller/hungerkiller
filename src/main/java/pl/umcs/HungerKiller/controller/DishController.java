package pl.umcs.HungerKiller.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.umcs.HungerKiller.openapi.api.DishApi;
import pl.umcs.HungerKiller.openapi.api.DishApiDelegate;
import pl.umcs.HungerKiller.openapi.model.DishDto;
import pl.umcs.HungerKiller.service.DishService;

@RestController
@RequestMapping(value = "/hungerkiller/v1")
public class DishController extends ControllerBase implements DishApi {
    private final DishService dishService;

    @Autowired
    public DishController(DishService dishService)
    {
        this.dishService = dishService;
    }

    @Override
    public DishApiDelegate getDelegate()
    {
        return dishService;
    }

    @Override
    public ResponseEntity<DishDto> addDish(DishDto dishDto) throws Exception
    {
        return dishService.addDish(dishDto);
    }
}
