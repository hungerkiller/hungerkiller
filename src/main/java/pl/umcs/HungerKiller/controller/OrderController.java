package pl.umcs.HungerKiller.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.umcs.HungerKiller.openapi.api.OrderApi;
import pl.umcs.HungerKiller.openapi.api.OrderApiDelegate;
import pl.umcs.HungerKiller.service.OrderService;

@RestController
@RequestMapping(value = "/hungerkiller/v1")
public class OrderController extends ControllerBase implements OrderApi {

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {this.orderService = orderService;}

    @Override
    public OrderApiDelegate getDelegate()
    {
        return orderService;
    }

}
