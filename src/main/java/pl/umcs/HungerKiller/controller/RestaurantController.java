package pl.umcs.HungerKiller.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.umcs.HungerKiller.openapi.api.RestaurantApi;
import pl.umcs.HungerKiller.openapi.api.RestaurantApiDelegate;
import pl.umcs.HungerKiller.service.RestaurantService;

@RestController
@RequestMapping(value = "/hungerkiller/v1")
public class RestaurantController extends ControllerBase implements RestaurantApi {

    private final RestaurantService restaurantService;

    @Autowired
    public RestaurantController(RestaurantService restaurantService)
    {
        this.restaurantService = restaurantService;
    }

    @Override
    public RestaurantApiDelegate getDelegate()
    {
        return restaurantService;
    }
}
