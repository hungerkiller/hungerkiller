package pl.umcs.HungerKiller.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.umcs.HungerKiller.exceptions.PasswordNotFitRequirementsException;
import pl.umcs.HungerKiller.openapi.api.UserApi;
import pl.umcs.HungerKiller.openapi.api.UserApiDelegate;
import pl.umcs.HungerKiller.service.UserService;

@RestController
@RequestMapping(value = "/hungerkiller/v1")
public class UserController extends ControllerBase implements UserApi {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService)
    {
        this.userService = userService;
    }

    @Override
    public UserApiDelegate getDelegate()
    {
        return userService;
    }

    @ExceptionHandler(PasswordNotFitRequirementsException.class)
    private ResponseEntity<?> handlePasswordNotFitRequirementsException(PasswordNotFitRequirementsException exception)
    {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
