package pl.umcs.HungerKiller.exceptions;

public class BodyHandlingException extends Exception {
    public BodyHandlingException(String message)
    {
        super(message);
    }
}
