package pl.umcs.HungerKiller.exceptions;

public class NotOwnerException extends Exception {
    public NotOwnerException(String message)
    {
        super(message);
    }
}

