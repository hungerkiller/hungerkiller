package pl.umcs.HungerKiller.exceptions;

public class PasswordNotFitRequirementsException extends Exception {
    public PasswordNotFitRequirementsException(String message)
    {
        super(message);
    }
}
