package pl.umcs.HungerKiller.exceptions;

public class WrongUserRoleException extends Exception {
    public WrongUserRoleException(String message)
    {
        super(message);
    }
}
