package pl.umcs.HungerKiller.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.umcs.HungerKiller.model.Address;
import pl.umcs.HungerKiller.openapi.model.AddressDto;

@Mapper
public interface AddressMapper {

    static AddressMapper getInstance()
    {
        return Mappers.getMapper(AddressMapper.class);
    }

    AddressDto dtoFromEntity(Address address);

    Address entityFromDto(AddressDto addressDto);
}
