package pl.umcs.HungerKiller.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import pl.umcs.HungerKiller.model.Deliverer;
import pl.umcs.HungerKiller.openapi.model.DelivererDto;

@Mapper(uses = {WorkHoursMapper.class})
public interface DelivererMapper {
    static DelivererMapper getInstance()
    {
        return Mappers.getMapper(DelivererMapper.class);
    }

    @Mapping(source = "workHours", target = "schedule")
    DelivererDto dtoFromEntity(Deliverer deliverer);

    @InheritInverseConfiguration(name = "dtoFromEntity")
    Deliverer entityFromDto(DelivererDto delivererDto);

    DelivererDto updateDtoFromEntity(Deliverer entity, @MappingTarget DelivererDto dto);

    Deliverer updateEntityFromDto(DelivererDto dto, @MappingTarget Deliverer entity);

}
