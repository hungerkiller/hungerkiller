package pl.umcs.HungerKiller.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import pl.umcs.HungerKiller.model.Dish;
import pl.umcs.HungerKiller.openapi.model.DishDto;

@Mapper(uses = {RestaurantWithoutDishesMapper.class})
public interface DishMapper {

    static DishMapper getInstance()
    {
        return Mappers.getMapper(DishMapper.class);
    }

    DishDto dtoFromEntity(Dish dish);

    Dish entityFromDto(DishDto dto);

    DishDto updateDtoFromEntity(Dish entity, @MappingTarget DishDto dto);

    @Mapping(source = "restaurant", target = "restaurant", ignore = true)
    Dish updateEntityFromDto(DishDto dto, @MappingTarget Dish entity);
}
