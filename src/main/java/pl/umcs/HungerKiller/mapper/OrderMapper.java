package pl.umcs.HungerKiller.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import pl.umcs.HungerKiller.model.Order;
import pl.umcs.HungerKiller.openapi.model.OrderDto;

import java.util.Date;

@Mapper(uses = {RestaurantWithoutDishesMapper.class, DishMapper.class, UserMapper.class})
public interface OrderMapper {

    static OrderMapper getInstance() { return Mappers.getMapper(OrderMapper.class); }

    @Mapping(source = "creation_time", target = "creationTime")
    OrderDto dtoFromEntity(Order order);

    @InheritInverseConfiguration(name = "dtoFromEntity")
    Order entityFromDto(OrderDto orderDto);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "restaurant", ignore = true)
    @Mapping(target = "dishes", ignore = true)
    @Mapping(target = "user", ignore = true)
    @Mapping(target = "creationTime", ignore = true)
    OrderDto updateDtoFromEntity(Order order, @MappingTarget OrderDto dto);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "restaurant", ignore = true)
    @Mapping(target = "dishes", ignore = true)
    @Mapping(target = "user", ignore = true)
    @Mapping(target = "creation_time", ignore = true)
    Order updateEntityFromDto(OrderDto dto, @MappingTarget Order order);

    default Long mapDate(Date date) { return date != null ? date.getTime() : null; }

    default Date mapDate(Long date) { return date != null ? new Date(date) : null; }

}
