package pl.umcs.HungerKiller.mapper;

import org.mapstruct.*;
import org.mapstruct.factory.Mappers;
import pl.umcs.HungerKiller.model.Restaurant;
import pl.umcs.HungerKiller.openapi.model.RestaurantDto;

import java.sql.Time;

@Mapper(uses = {AddressMapper.class, DishMapper.class})
public interface RestaurantMapper {

    static RestaurantMapper getInstance()
    {
        return Mappers.getMapper(RestaurantMapper.class);
    }

    @Mapping(source = "openFrom", target = "openFromTime")
    @Mapping(source = "openTill", target = "openTillTime")
    RestaurantDto dtoFromEntity(Restaurant restaurant);

    @InheritInverseConfiguration(name = "dtoFromEntity")
    Restaurant entityFromDto(RestaurantDto restaurantDto);

    @InheritConfiguration(name = "dtoFromEntity")
    RestaurantDto updateDtoFromEntity(Restaurant entity, @MappingTarget RestaurantDto dto);

    @InheritInverseConfiguration(name = "dtoFromEntity")
    Restaurant updateEntityFromDto(RestaurantDto dto, @MappingTarget Restaurant entity);


    // Helper methods that provide mapping non-obvious for MapStruct

    default String map(Time time)
    {
        return time != null ? time.toString() : null;
    }

    default Time map(String timeStr)
    {
        return timeStr != null ? Time.valueOf(timeStr) : null;
    }

}
