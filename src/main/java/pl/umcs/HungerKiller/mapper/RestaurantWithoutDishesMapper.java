package pl.umcs.HungerKiller.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import pl.umcs.HungerKiller.model.Restaurant;
import pl.umcs.HungerKiller.openapi.model.RestaurantDto;

@Mapper
public interface RestaurantWithoutDishesMapper extends RestaurantMapper {
    static RestaurantMapper getInstance()
    {
        return Mappers.getMapper(RestaurantMapper.class);
    }

    @Override
    @Mapping(source = "openFrom", target = "openFromTime")
    @Mapping(source = "openTill", target = "openTillTime")
    @Mapping(source = "dishes", target = "dishes", ignore = true)
    RestaurantDto dtoFromEntity(Restaurant restaurant);
}