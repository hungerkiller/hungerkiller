package pl.umcs.HungerKiller.mapper;

import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import pl.umcs.HungerKiller.model.User;
import pl.umcs.HungerKiller.openapi.model.UserDto;

@Mapper
public interface UserMapper {
    static UserMapper getInstance() { return Mappers.getMapper(UserMapper.class); }

    @Mapping(source = "userRole", target = "role")
    UserDto dtoFromEntity(User user);

    @Mapping(source = "role", target = "userRole")
    User entityFromDto(UserDto userDto);

    @InheritConfiguration(name = "entityFromDto")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "password", ignore = true)
    User updateEntityFromDto(UserDto dto, @MappingTarget User entity);

    default User.UserRole map(UserDto.RoleEnum role)
    {
        String roleValue = role.getValue();
        return User.UserRole.valueOf("ur" + roleValue.substring(0, 1).toUpperCase() + roleValue.substring(1));
    }

    default UserDto.RoleEnum map(User.UserRole role)
    {
        String roleValue = role.toString();
        return UserDto.RoleEnum.fromValue(roleValue.substring(2).toLowerCase());
    }

}
