package pl.umcs.HungerKiller.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import pl.umcs.HungerKiller.model.WorkHours;
import pl.umcs.HungerKiller.openapi.model.WorkHoursDto;

import java.sql.Time;

@Mapper
public interface WorkHoursMapper {
    static WorkHoursMapper getInstance()
    {
        return Mappers.getMapper(WorkHoursMapper.class);
    }


    WorkHoursDto dtoFromEntity(WorkHours workHours);

    WorkHours entityFromDto(WorkHoursDto dto);

    WorkHoursDto updateDtoFromEntity(WorkHours entity, @MappingTarget WorkHoursDto dto);

    WorkHours updateEntityFromDto(WorkHoursDto dto, @MappingTarget WorkHours entity);

    default String map(Time time)
    {
        return time != null ? time.toString() : null;
    }

    default Time map(String timeStr)
    {
        return timeStr != null ? Time.valueOf(timeStr) : null;
    }

}
