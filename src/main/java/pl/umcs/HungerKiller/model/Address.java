package pl.umcs.HungerKiller.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "hk_addresses")
@Getter
@Setter
@ToString
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "street")
    private String street;

    @Column(name = "town")
    private String town;

    @Column(name = "zipcode")
    private String zipcode;

    public Address()
    {
    }

    public Address(String street, String town, String zipcode)
    {
        this.street = street;
        this.town = town;
        this.zipcode = zipcode;
    }
}
