package pl.umcs.HungerKiller.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Entity
@Table(name = "hk_deliverers")
@Getter
@Setter
@ToString
public class Deliverer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "service_in_city")
    private String serviceInCity;

    @OneToMany(targetEntity = WorkHours.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "deliverer_id", nullable = false)
    private List<WorkHours> workHours;

    @Column(name = "user_id")
    private Long userId;

    public List<WorkHours> getWorkHoursForToday()
    {
        List<WorkHours> result = new ArrayList<>();
        WorkHours.DayOfWeek
                dayOfWeek =
                WorkHours.DayOfWeek.valueOf(DateTime.now().dayOfWeek().getAsText(Locale.ENGLISH).toUpperCase());
        for (WorkHours wh : workHours) {
            if (wh.getDayOfWeek() == dayOfWeek) {
                result.add(wh);
            }
        }

        return result;
    }
}
