package pl.umcs.HungerKiller.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "hk_dishes")
@Getter
@Setter
public class Dish {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "price", precision = 10, scale = 2)
    private Double price;

    @ManyToOne(targetEntity = Restaurant.class)
    private Restaurant restaurant;

    @Column(name = "owner_id")
    private Long ownerId;

    public Dish()
    {

    }

    @Override
    public String toString()
    {
        return "Dish{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", restaurantId=" + restaurant.getId() +
                ", ownerId=" + ownerId +
                '}';
    }
}
