package pl.umcs.HungerKiller.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "hk_orders")
@Getter
@Setter
@ToString
public class Order {
    public enum Status {
        IN_REALIZATION,
        DELIVERY,
        DONE,
        CANCELLED,
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(targetEntity = Restaurant.class)
    private Restaurant restaurant;

    @OneToMany(targetEntity = Dish.class, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name = "hk_order_dishes",
            joinColumns = {@JoinColumn(name = "order_id", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "dish_id", nullable = false)}
    )
    private List<Dish> dishes;

    @ManyToOne(targetEntity = User.class)
    private User user;

    @Column(name = "deliverer_id")
    private Long delivererId;

    @Column(name = "creation_time")
    @CreationTimestamp
    private Date creation_time;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

}
