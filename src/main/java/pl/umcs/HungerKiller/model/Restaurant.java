package pl.umcs.HungerKiller.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Time;
import java.util.List;

@Entity
@Table(name = "hk_restaurants")
@Getter
@Setter
public class Restaurant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "open_from")
    private Time openFrom;

    @Column(name = "open_till")
    private Time openTill;

    @OneToOne(targetEntity = Address.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", referencedColumnName = "id", nullable = false)
    private Address address;

    @Column(name = "phone_number")
    private String phoneNumber;

    @OneToMany(targetEntity = Dish.class, mappedBy = "restaurant", cascade = CascadeType.ALL)
    private List<Dish> dishes;

    @OneToMany(targetEntity = Order.class, mappedBy = "restaurant", cascade = CascadeType.ALL)
    private List<Order> orders;

    @Column(name = "owner_id")
    private Long ownerId;

    @Override
    public String toString()
    {
        return "Restaurant{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", openFrom=" + openFrom +
                ", openTill=" + openTill +
                ", address=" + address +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", ownerId=" + ownerId +
                ", dishes=" + dishes +
                '}';
    }
}
