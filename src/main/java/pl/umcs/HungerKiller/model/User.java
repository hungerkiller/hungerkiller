package pl.umcs.HungerKiller.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "hk_users")
@Getter
@Setter
public class User {
    public enum UserRole {
        urRestaurateur,
        urDeliverer,
        urCustomer
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "phone_number")
    private String phoneNumber;

    @OneToOne(targetEntity = Address.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "delivery_address_id", referencedColumnName = "id", nullable = false)
    private Address deliveryAddress;

    @Column(name = "role")
    @Enumerated(value = EnumType.STRING)
    private UserRole userRole;

    @Column(name = "enabled")
    private Character enabled;

    @OneToMany(targetEntity = Order.class, cascade = CascadeType.ALL, mappedBy = "user")
    private List<Order> ordersList;

    @Override
    public String toString()
    {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", deliveryAddress=" + deliveryAddress +
                ", userRole=" + userRole +
                ", enabled=" + enabled +
                '}';
    }

}
