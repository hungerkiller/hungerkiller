package pl.umcs.HungerKiller.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Time;

@Entity
@Table(name = "hk_deliverer_schedule")
@Getter
@Setter
public class WorkHours {
    public enum DayOfWeek {
        SUNDAY,
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "day_of_week")
    @Enumerated(EnumType.STRING)
    private DayOfWeek dayOfWeek;

//    @ManyToOne(targetEntity = Deliverer.class)
//    private Deliverer deliverer;

    @Column(name = "time_start")
    private Time timeStart;

    @Column(name = "time_end")
    private Time timeEnd;

    @Override
    public String toString()
    {
        return "WorkHours{" +
                "id=" + id +
                ", dayOfWeek=" + dayOfWeek +
//                ", delivererId=" + deliverer.getId() +
                ", timeStart=" + timeStart +
                ", timeEnd=" + timeEnd +
                '}';
    }

}
