package pl.umcs.HungerKiller.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.umcs.HungerKiller.model.Address;

public interface AddressRepository extends JpaRepository<Address, Long> {

}
