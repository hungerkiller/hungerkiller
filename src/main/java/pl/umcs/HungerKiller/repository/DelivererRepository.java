package pl.umcs.HungerKiller.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.umcs.HungerKiller.model.Deliverer;

import java.util.List;

public interface DelivererRepository extends JpaRepository<Deliverer, Long> {
    List<Deliverer> findAllByServiceInCity(String serviceInCity);

    Deliverer findDelivererByUserId(Long userId);
}
