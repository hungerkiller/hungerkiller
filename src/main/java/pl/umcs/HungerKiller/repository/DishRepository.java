package pl.umcs.HungerKiller.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.umcs.HungerKiller.model.Dish;

import java.util.List;
import java.util.Optional;

@Repository
public interface DishRepository extends JpaRepository<Dish, Long> {
    List<Dish> findAllByNameStartsWith(String name);

    List<Dish> findAllByRestaurant_Id(Long restaurantId);

    Optional<Dish> findFirstByNameStartingWith(String name);

    Optional<Dish> findFirstByRestaurantId(Long restaurant_id);
}
