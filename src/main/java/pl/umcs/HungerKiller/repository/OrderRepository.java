package pl.umcs.HungerKiller.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.umcs.HungerKiller.model.Order;

import java.sql.Time;
import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    List<Order> findAllByUserId(Long userId);

    @Query(value = """
            SELECT DISTINCT ord.*
            FROM hk_orders ord,
                 hk_restaurants rest,
                 hk_addresses addr
            WHERE DATE(creation_time) = CURRENT_DATE()
              AND :dayOfWeek = DAYOFWEEK(CURRENT_DATE())
              AND TIME(creation_time) > :startTime
              AND TIME(creation_time) < :endTime
              AND status = 'IN_REALIZATION'
              AND rest.id = ord.restaurant_id
              AND addr.town = :town ;""", nativeQuery = true)
    List<Order> findAllOrdersInDelivererWorkHours(Time startTime, Time endTime, String town, int dayOfWeek);

    List<Order> findAllByDelivererIdAndStatus(Long delivererId,
                                              Order.Status status);
}
