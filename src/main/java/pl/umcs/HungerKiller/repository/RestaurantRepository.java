package pl.umcs.HungerKiller.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.umcs.HungerKiller.model.Restaurant;

import java.util.List;
import java.util.Optional;

@Repository
public interface RestaurantRepository extends JpaRepository<Restaurant, Long> {
    Optional<Restaurant> findFirstByOwnerIdAndName(Long ownerId, String name);

    List<Restaurant> findAllByNameStartsWith(String name);

    Optional<Restaurant> findFirstByNameStartingWith(String name);
}