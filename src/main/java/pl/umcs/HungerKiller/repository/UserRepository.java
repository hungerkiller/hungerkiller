package pl.umcs.HungerKiller.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.umcs.HungerKiller.model.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> getUserByEmail(String email);
}
