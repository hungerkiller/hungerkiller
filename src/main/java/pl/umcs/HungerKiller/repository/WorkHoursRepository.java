package pl.umcs.HungerKiller.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.umcs.HungerKiller.model.WorkHours;

import java.util.List;

public interface WorkHoursRepository extends JpaRepository<WorkHours, Long> {
    List<WorkHours> findAllByDayOfWeekEquals(WorkHours.DayOfWeek dayOfWeek);
}
