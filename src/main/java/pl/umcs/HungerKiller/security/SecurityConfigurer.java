package pl.umcs.HungerKiller.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import pl.umcs.HungerKiller.security.filters.JwtRequestFilter;
import pl.umcs.HungerKiller.security.services.SecUserService;

@EnableWebSecurity
public class SecurityConfigurer extends WebSecurityConfigurerAdapter {

    private final SecUserService userService;
    private final JwtRequestFilter filter;

    @Autowired
    public SecurityConfigurer(SecUserService userService, JwtRequestFilter filter)
    {
        super();
        this.userService = userService;
        this.filter = filter;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception
    {
        auth.userDetailsService(userService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http.csrf().disable().cors().and()
            .authorizeRequests()

            .antMatchers(HttpMethod.GET, "/hungerkiller/v1/dish").permitAll()
            .antMatchers(HttpMethod.GET, "/hungerkiller/v1/dish/id/*").permitAll()
            .antMatchers(HttpMethod.GET, "/hungerkiller/v1/dish/name/*").permitAll()
            .antMatchers(HttpMethod.GET, "/hungerkiller/v1/restaurant").permitAll()
            .antMatchers(HttpMethod.GET, "/hungerkiller/v1/restaurant/id/*").permitAll()
            .antMatchers(HttpMethod.GET, "/hungerkiller/v1/restaurant/name/*").permitAll()
            .antMatchers(HttpMethod.GET, "/hungerkiller/v1/restaurant/id/*/dish").permitAll()
            .antMatchers(HttpMethod.POST, "/hungerkiller/v1/user/login").permitAll()
            .antMatchers(HttpMethod.POST, "/hungerkiller/v1/user").permitAll()

            .anyRequest().authenticated()
            .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        ;

        http.addFilterBefore(filter, UsernamePasswordAuthenticationFilter.class);
    }


    @Bean
    public PasswordEncoder passwordEncoder()
    {
        return new BCryptPasswordEncoder(5);
    }


    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception
    {
        return super.authenticationManagerBean();
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource()
    {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return source;
    }

}
