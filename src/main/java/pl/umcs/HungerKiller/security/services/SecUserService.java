package pl.umcs.HungerKiller.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.umcs.HungerKiller.model.User;
import pl.umcs.HungerKiller.repository.UserRepository;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class SecUserService implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public SecUserService(UserRepository userRepository)
    {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException
    {
        final Optional<User> user = userRepository.getUserByEmail(email);
        if (user.isEmpty()) {
            throw new UsernameNotFoundException("Invalid e-mail provided");
        }

        return new org.springframework.security.core.userdetails.User(user.get().getEmail(),
                                                                      user.get().getPassword(),
                                                                      new ArrayList<>());
    }
}
