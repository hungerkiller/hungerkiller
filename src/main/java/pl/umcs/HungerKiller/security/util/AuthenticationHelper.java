package pl.umcs.HungerKiller.security.util;

import org.springframework.security.core.Authentication;
import pl.umcs.HungerKiller.model.User;

public interface AuthenticationHelper {
    Authentication getAuthentication();

    User getCurrentLoggedInUser();

    default String getCurrentLoggedInUserEmail()
    {
        return getAuthentication().getName();
    }
}
