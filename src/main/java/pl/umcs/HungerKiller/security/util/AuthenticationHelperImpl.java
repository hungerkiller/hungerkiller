package pl.umcs.HungerKiller.security.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import pl.umcs.HungerKiller.model.User;
import pl.umcs.HungerKiller.repository.UserRepository;

@Component
public class AuthenticationHelperImpl implements AuthenticationHelper {
    UserRepository userRepository;

    @Autowired
    public AuthenticationHelperImpl(UserRepository userRepository)
    {
        this.userRepository = userRepository;
    }

    @Override
    public Authentication getAuthentication()
    {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @Override
    public User getCurrentLoggedInUser()
    {
        return userRepository.getUserByEmail(getCurrentLoggedInUserEmail()).get();
    }
}
