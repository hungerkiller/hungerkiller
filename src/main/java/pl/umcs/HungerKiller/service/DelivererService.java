package pl.umcs.HungerKiller.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.umcs.HungerKiller.exceptions.NotOwnerException;
import pl.umcs.HungerKiller.exceptions.WrongUserRoleException;
import pl.umcs.HungerKiller.mapper.DelivererMapper;
import pl.umcs.HungerKiller.mapper.OrderMapper;
import pl.umcs.HungerKiller.mapper.WorkHoursMapper;
import pl.umcs.HungerKiller.model.Deliverer;
import pl.umcs.HungerKiller.model.Order;
import pl.umcs.HungerKiller.model.User;
import pl.umcs.HungerKiller.model.WorkHours;
import pl.umcs.HungerKiller.openapi.api.DelivererApiDelegate;
import pl.umcs.HungerKiller.openapi.model.DelivererDto;
import pl.umcs.HungerKiller.openapi.model.OrderDto;
import pl.umcs.HungerKiller.openapi.model.WorkHoursDto;
import pl.umcs.HungerKiller.repository.DelivererRepository;
import pl.umcs.HungerKiller.repository.OrderRepository;
import pl.umcs.HungerKiller.repository.WorkHoursRepository;
import pl.umcs.HungerKiller.security.util.AuthenticationHelper;

import java.util.*;

@Service
public class DelivererService implements DelivererApiDelegate {
    private final DelivererRepository delivererRepository;
    private final WorkHoursRepository workHoursRepository;
    private final OrderRepository orderRepository;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public DelivererService(DelivererRepository delivererRepository,
                            WorkHoursRepository workHoursRepository,
                            OrderRepository orderRepository,
                            AuthenticationHelper authenticationHelper)
    {
        this.delivererRepository = delivererRepository;
        this.workHoursRepository = workHoursRepository;
        this.orderRepository = orderRepository;
        this.authenticationHelper = authenticationHelper;
    }

    @Override
    public ResponseEntity<DelivererDto> getDeliverer() throws Exception
    {
        if (authenticationHelper.getCurrentLoggedInUser().getUserRole() != User.UserRole.urDeliverer) {
            throw new WrongUserRoleException("Currently logged in user is not deliverer");
        }
        Long currentUserId = authenticationHelper.getCurrentLoggedInUser().getId();
        Deliverer deliverer = delivererRepository.findDelivererByUserId(currentUserId);
        return new ResponseEntity<>(DelivererMapper.getInstance().dtoFromEntity(deliverer), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<OrderDto>> getDelivererJobOffers() throws Exception
    {
        if (authenticationHelper.getCurrentLoggedInUser().getUserRole() != User.UserRole.urDeliverer) {
            throw new WrongUserRoleException("Currently logged in user is not deliverer");
        }
        Long currentUserId = authenticationHelper.getCurrentLoggedInUser().getId();
        Deliverer deliverer = delivererRepository.findDelivererByUserId(currentUserId);
        Set<Order> ordersForDeliverer = new HashSet<>();

        for (WorkHours workHours : deliverer.getWorkHoursForToday()) {
            ordersForDeliverer.addAll(orderRepository
                                              .findAllOrdersInDelivererWorkHours(workHours.getTimeStart(),
                                                                                 workHours.getTimeEnd(),
                                                                                 deliverer.getServiceInCity(),
                                                                                 sqlDate(workHours.getDayOfWeek())));
        }

        List<OrderDto> result = new ArrayList<>();
        ordersForDeliverer.forEach(order -> result.add(OrderMapper.getInstance().dtoFromEntity(order)));

        return new ResponseEntity<>(result, HttpStatus.OK);

    }

    private int sqlDate(WorkHours.DayOfWeek dayOfWeek)
    {
        return dayOfWeek.ordinal() + 1; // sunday = 1,... ,sat=7
    }

    @Override
    public ResponseEntity<DelivererDto> updateDeliverer(DelivererDto delivererDto) throws Exception
    {
        if (authenticationHelper.getCurrentLoggedInUser().getUserRole() != User.UserRole.urDeliverer) {
            throw new WrongUserRoleException("Currently logged in user is not deliverer");
        }
        Optional<Deliverer> deliverer = delivererRepository.findById(delivererDto.getId());
        if (deliverer.isEmpty() || deliverer.get().getUserId() == null) {
            throw new ResourceNotFoundException("Deliverer with id=" + delivererDto
                    .getId() + " does not exist in database.");
        }
        if (!deliverer.get().getUserId().equals(authenticationHelper.getCurrentLoggedInUser().getId())) {
            throw new NotOwnerException("Currently logged in deliverer tried changed another deliverer.");
        }

        Deliverer updated = DelivererMapper.getInstance().entityFromDto(delivererDto);
        updated.setUserId(deliverer.get().getUserId());
        updated = delivererRepository.save(updated);

        return new ResponseEntity<>(DelivererMapper.getInstance().dtoFromEntity(updated), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<WorkHoursDto>> getDelivererWorkHours() throws Exception
    {
        if (authenticationHelper.getCurrentLoggedInUser().getUserRole() != User.UserRole.urDeliverer) {
            throw new WrongUserRoleException("Currently logged in user is not deliverer");
        }
        Long currentUserId = authenticationHelper.getCurrentLoggedInUser().getId();
        Deliverer deliverer = delivererRepository.findDelivererByUserId(currentUserId);

        List<WorkHoursDto> result = new ArrayList<>();
        deliverer.getWorkHours().forEach(wh -> result.add(WorkHoursMapper.getInstance().dtoFromEntity(wh)));


        return new ResponseEntity<>(result, HttpStatus.OK);

    }

    @Override
    public ResponseEntity<WorkHoursDto> updateDelivererWorkHour(WorkHoursDto workHoursDto) throws Exception
    {
        if (authenticationHelper.getCurrentLoggedInUser().getUserRole() != User.UserRole.urDeliverer) {
            throw new WrongUserRoleException("Currently logged in user is not deliverer");
        }
        workHoursRepository.save(WorkHoursMapper.getInstance().entityFromDto(workHoursDto));
        return new ResponseEntity<>(workHoursDto, HttpStatus.OK);

    }

    @Override
    public ResponseEntity<WorkHoursDto> createNewDelivererWorkHour(WorkHoursDto workHoursDto) throws Exception
    {
        if (authenticationHelper.getCurrentLoggedInUser().getUserRole() != User.UserRole.urDeliverer) {
            throw new WrongUserRoleException("Currently logged in user is not deliverer");
        }
        Long currentUserId = authenticationHelper.getCurrentLoggedInUser().getId();
        Deliverer deliverer = delivererRepository.findDelivererByUserId(currentUserId);

        List<WorkHours> workHoursList = deliverer.getWorkHours();
        workHoursList.add(WorkHoursMapper.getInstance().entityFromDto(workHoursDto));

        deliverer.setWorkHours(workHoursList);
        delivererRepository.save(deliverer);

        return new ResponseEntity<>(workHoursDto, HttpStatus.OK);

    }

    @Override
    public ResponseEntity<Void> removeWorkHour(Long workHourId) throws Exception
    {
        if (authenticationHelper.getCurrentLoggedInUser().getUserRole() != User.UserRole.urDeliverer) {
            throw new WrongUserRoleException("Currently logged in user is not deliverer");
        }

        Optional<WorkHours> workHours = workHoursRepository.findById(workHourId);
        if (workHours.isEmpty()) {
            throw new ResourceNotFoundException("WorkHours with id=" + workHourId + " not exists in database.");
        }

        workHoursRepository.deleteById(workHourId);
        return new ResponseEntity<>(HttpStatus.OK);

    }

    @Override
    public ResponseEntity<List<OrderDto>> getDelivererOrders() throws Exception
    {
        if (authenticationHelper.getCurrentLoggedInUser().getUserRole() != User.UserRole.urDeliverer) {
            throw new WrongUserRoleException("Currently logged in user is not deliverer");
        }
        Long currentUserId = authenticationHelper.getCurrentLoggedInUser().getId();
        Deliverer deliverer = delivererRepository.findDelivererByUserId(currentUserId);
        Long currentDelivererId = deliverer.getId();
        List<Order> delivererOrders = orderRepository.findAllByDelivererIdAndStatus(currentDelivererId,
                                                                                    Order.Status.DELIVERY);
        List<OrderDto> result = new ArrayList<>();
        delivererOrders.forEach(order -> result.add(OrderMapper.getInstance().dtoFromEntity(order)));

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
