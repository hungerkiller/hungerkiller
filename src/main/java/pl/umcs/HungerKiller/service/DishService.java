package pl.umcs.HungerKiller.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.umcs.HungerKiller.exceptions.BodyHandlingException;
import pl.umcs.HungerKiller.exceptions.NotOwnerException;
import pl.umcs.HungerKiller.mapper.DishMapper;
import pl.umcs.HungerKiller.model.Dish;
import pl.umcs.HungerKiller.model.Restaurant;
import pl.umcs.HungerKiller.openapi.api.DishApiDelegate;
import pl.umcs.HungerKiller.openapi.model.DishDto;
import pl.umcs.HungerKiller.repository.DishRepository;
import pl.umcs.HungerKiller.repository.RestaurantRepository;
import pl.umcs.HungerKiller.security.util.AuthenticationHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DishService implements DishApiDelegate {
    private final RestaurantRepository restaurantRepository;
    private final DishRepository dishRepository;
    private final AuthenticationHelper authenticationHelper;


    @Autowired
    public DishService(RestaurantRepository restaurantRepository,
                       DishRepository dishRepository,
                       AuthenticationHelper authenticationHelper)
    {
        this.restaurantRepository = restaurantRepository;
        this.dishRepository = dishRepository;
        this.authenticationHelper = authenticationHelper;
    }


    @Override
    public ResponseEntity<DishDto> addDish(DishDto dishDto) throws Exception
    {
        if (dishDto.getId() != null) {
            throw new BodyHandlingException("Given object got id. It could override exiting one. Aborted.");
        }

        Dish dish = DishMapper.getInstance().entityFromDto(dishDto);

        Optional<Restaurant> restaurant = restaurantRepository.findById(dishDto.getRestaurant().getId());

        if (restaurant.isEmpty()) {
            throw new ResourceNotFoundException("Restaurant with id=" + dishDto.getRestaurant()
                                                                               .getId() + " not exists in database.");
        }
        if (restaurant.get().getOwnerId() != null &&
                !authenticationHelper.getCurrentLoggedInUser().getId().equals(restaurant.get().getOwnerId())) {
            throw new NotOwnerException(
                    "No access to modify given object. Currently logged in user is not restaurant's owner.");
        }

        dish.setRestaurant(restaurant.get());
        dish.setOwnerId(authenticationHelper.getCurrentLoggedInUser().getId());

        dish = dishRepository.save(dish);

        return new ResponseEntity<>(DishMapper.getInstance().dtoFromEntity(dish), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<DishDto> getDishById(Long dishId)
    {
        Optional<Dish> dish = dishRepository.findById(dishId);
        if (dish.isEmpty()) {
            throw new ResourceNotFoundException("Restaurant with id=" + dishId + " not exists in database.");
        }

        return new ResponseEntity<>(DishMapper.getInstance().dtoFromEntity(dish.get()), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<DishDto>> getDishByName(String dishName)
    {
        List<Dish> dishes = dishRepository.findAllByNameStartsWith(dishName);

        if (dishes.isEmpty()) {
            throw new ResourceNotFoundException("Dishes with name=" + dishName + " not exists in database.");
        }

        List<DishDto> result = new ArrayList<>();
        for (Dish dish : dishes) {
            result.add(DishMapper.getInstance().dtoFromEntity(dish));
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<DishDto>> getDishList()
    {
        List<Dish> dishes = dishRepository.findAll();

        List<DishDto> result = new ArrayList<>();
        dishes.forEach(dish -> result.add(DishMapper.getInstance().dtoFromEntity(dish)));

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> removeDish(Long dishId) throws NotOwnerException
    {
        Optional<Dish> dish = dishRepository.findById(dishId);
        if (dish.isEmpty()) {
            throw new ResourceNotFoundException("Dish with id=" + dishId + " not exists in database.");
        }
        if (dish.get().getOwnerId() != null &&
                !authenticationHelper.getCurrentLoggedInUser().getId().equals(dish.get().getOwnerId())) {
            throw new NotOwnerException(
                    "No access to modify given object. Currently logged in user is not restaurant's owner.");
        }

        dishRepository.deleteById(dishId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<DishDto> updateDish(DishDto dishDto) throws NotOwnerException
    {
        Optional<Dish> dish = dishRepository.findById(dishDto.getId());
        if (dish.isEmpty()) {
            throw new ResourceNotFoundException("Dish with id=" + dishDto.getId() + " not exists in database.");
        }
        if (dish.get().getOwnerId() != null &&
                !authenticationHelper.getCurrentLoggedInUser().getId().equals(dish.get().getOwnerId())) {
            throw new NotOwnerException(
                    "No access to modify given object. Currently logged in user is not restaurant's owner.");
        }

        Dish updated = DishMapper.getInstance().updateEntityFromDto(dishDto, dish.get());

        updated = dishRepository.save(updated);

        return new ResponseEntity<>(DishMapper.getInstance().dtoFromEntity(updated), HttpStatus.OK);
    }
}
