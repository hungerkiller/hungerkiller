package pl.umcs.HungerKiller.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.umcs.HungerKiller.exceptions.BodyHandlingException;
import pl.umcs.HungerKiller.exceptions.NotOwnerException;
import pl.umcs.HungerKiller.exceptions.WrongUserRoleException;
import pl.umcs.HungerKiller.mapper.OrderMapper;
import pl.umcs.HungerKiller.model.Deliverer;
import pl.umcs.HungerKiller.model.Order;
import pl.umcs.HungerKiller.model.Restaurant;
import pl.umcs.HungerKiller.model.User;
import pl.umcs.HungerKiller.openapi.api.OrderApiDelegate;
import pl.umcs.HungerKiller.openapi.model.DishDto;
import pl.umcs.HungerKiller.openapi.model.OrderDto;
import pl.umcs.HungerKiller.repository.DelivererRepository;
import pl.umcs.HungerKiller.repository.DishRepository;
import pl.umcs.HungerKiller.repository.OrderRepository;
import pl.umcs.HungerKiller.repository.RestaurantRepository;
import pl.umcs.HungerKiller.security.util.AuthenticationHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService implements OrderApiDelegate {

    private final OrderRepository orderRepository;
    private final RestaurantRepository restaurantRepository;
    private final DishRepository dishRepository;
    private final DelivererRepository delivererRepository;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public OrderService(OrderRepository orderRepository,
                        RestaurantRepository restaurantRepository,
                        DishRepository dishRepository,
                        DelivererRepository delivererRepository,
                        AuthenticationHelper authenticationHelper)
    {
        this.orderRepository = orderRepository;
        this.restaurantRepository = restaurantRepository;
        this.dishRepository = dishRepository;
        this.delivererRepository = delivererRepository;
        this.authenticationHelper = authenticationHelper;
    }

    @Override
    public ResponseEntity<List<OrderDto>> getUserOrders()
    {
        Long currentUserId = authenticationHelper.getCurrentLoggedInUser().getId();
        List<Order> orders = orderRepository.findAllByUserId(currentUserId);
        if (orders.isEmpty()) {
            throw new ResourceNotFoundException("No orders.");
        }
        List<OrderDto> result = new ArrayList<>();
        orders.forEach(order -> result.add(OrderMapper.getInstance().dtoFromEntity(order)));

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<OrderDto> addOrder(OrderDto orderDto) throws Exception
    {
        User user = authenticationHelper.getCurrentLoggedInUser();
        if (user.getUserRole() != User.UserRole.urCustomer) {
            throw new WrongUserRoleException("Only customers can add order");
        }
        Order order = OrderMapper.getInstance().entityFromDto(orderDto);
        Restaurant restaurant = restaurantRepository.getOne(orderDto.getRestaurant().getId());
        order.setRestaurant(restaurant);

        order.getDishes().clear();
        for (DishDto dto : orderDto.getDishes()) {
            order.getDishes().add(dishRepository.getOne(dto.getId()));
        }

        order.setUser(user);
        order.setStatus(Order.Status.IN_REALIZATION);
        order = orderRepository.save(order);

        return new ResponseEntity<>(OrderMapper.getInstance().dtoFromEntity(order), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<OrderDto> updateOrder(OrderDto orderDto) throws Exception
    {
        Optional<Order> order = orderRepository.findById(orderDto.getId());
        if (order.isEmpty()) {
            throw new ResourceNotFoundException("Order with id=" + orderDto.getId() + " not exists in database.");
        }
        User currentUser = authenticationHelper.getCurrentLoggedInUser();

        if (currentUser.getUserRole() != User.UserRole.urDeliverer) {
            throw new WrongUserRoleException("No access to update order. Only deliverers can update orders.");
        }

        Deliverer currentlyLoggedDeliverer = delivererRepository.findDelivererByUserId(currentUser.getId());

        if (order.get().getDelivererId() != null) {

            if (!currentlyLoggedDeliverer.getId().equals(order.get().getDelivererId())) {
                throw new NotOwnerException(
                        "No access to update given object. Currently logged in user is not order's deliverer.");
            }
        }

        if (!orderDto.getDelivererId().equals(currentlyLoggedDeliverer.getId())) {
            throw new BodyHandlingException("It's forbidden to set a deliverer other than yourself");
        }

        Order updated = OrderMapper.getInstance().updateEntityFromDto(orderDto, order.get());
        updated = orderRepository.save(updated);

        return new ResponseEntity<>(OrderMapper.getInstance().dtoFromEntity(updated), HttpStatus.OK);

    }
}
