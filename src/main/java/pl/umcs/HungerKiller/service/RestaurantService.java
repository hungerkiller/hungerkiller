package pl.umcs.HungerKiller.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.umcs.HungerKiller.exceptions.BodyHandlingException;
import pl.umcs.HungerKiller.exceptions.NotOwnerException;
import pl.umcs.HungerKiller.exceptions.WrongUserRoleException;
import pl.umcs.HungerKiller.mapper.DishMapper;
import pl.umcs.HungerKiller.mapper.RestaurantMapper;
import pl.umcs.HungerKiller.model.Dish;
import pl.umcs.HungerKiller.model.Restaurant;
import pl.umcs.HungerKiller.model.User;
import pl.umcs.HungerKiller.openapi.api.RestaurantApiDelegate;
import pl.umcs.HungerKiller.openapi.model.DishDto;
import pl.umcs.HungerKiller.openapi.model.RestaurantDto;
import pl.umcs.HungerKiller.repository.DishRepository;
import pl.umcs.HungerKiller.repository.RestaurantRepository;
import pl.umcs.HungerKiller.security.util.AuthenticationHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RestaurantService implements RestaurantApiDelegate {

    private final RestaurantRepository restaurantRepository;
    private final DishRepository dishRepository;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public RestaurantService(RestaurantRepository restaurantRepository,
                             DishRepository dishRepository,
                             AuthenticationHelper authenticationHelper)
    {
        this.restaurantRepository = restaurantRepository;
        this.dishRepository = dishRepository;
        this.authenticationHelper = authenticationHelper;
    }

    @Override
    public ResponseEntity<DishDto> addDishForRestaurant(Long restaurantId, DishDto dishDto) throws Exception
    {
        if (dishDto.getId() != null) {
            throw new BodyHandlingException("Given object got id. It could override exiting one. Aborted.");
        }
        Optional<Restaurant> restaurant = restaurantRepository.findById(restaurantId);
        if (restaurant.isEmpty()) {
            throw new ResourceNotFoundException("Restaurant with id=" + restaurantId + " not exists in database.");
        }
        // If currently logged in user is not creator of restaurant shouldn't be able to delete or modify.
        if (restaurant.get().getOwnerId() != null &&
                !authenticationHelper.getCurrentLoggedInUser().getId().equals(restaurant.get().getOwnerId())) {
            throw new NotOwnerException(
                    "No access to modify given object. Currently logged in user is not restaurant's owner.");
        }
        Dish dish = DishMapper.getInstance().entityFromDto(dishDto);
        dish.setRestaurant(restaurant.get());
        dish = dishRepository.save(dish);

        return new ResponseEntity<>(DishMapper.getInstance().dtoFromEntity(dish), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<RestaurantDto> addRestaurant(RestaurantDto restaurantDto) throws Exception
    {
        if (restaurantDto.getId() != null) {
            throw new BodyHandlingException("Given Restaurant object got id. It could override exiting one. Aborted.");
        }
        if (restaurantDto.getAddress().getId() != null) {
            throw new BodyHandlingException("Given Restaurant object got Address id. It could not using exiting one.");
        }
        if (authenticationHelper.getCurrentLoggedInUser().getUserRole() != User.UserRole.urRestaurateur) {
            throw new WrongUserRoleException("Unable to create new restaurant. Invalid user's role.");
        }

        try {
            Restaurant restaurant = RestaurantMapper.getInstance().entityFromDto(restaurantDto);
            restaurant.setOwnerId(authenticationHelper.getCurrentLoggedInUser().getId());
            restaurant = restaurantRepository.save(restaurant);
            return new ResponseEntity<>(RestaurantMapper.getInstance().dtoFromEntity(restaurant), HttpStatus.CREATED);
        } catch (DataIntegrityViolationException | IllegalArgumentException e) {
            throw new BodyHandlingException("Not valid data is given.");
        }
    }

    @Override
    public ResponseEntity<List<DishDto>> getDishesFromRestaurant(Long restaurantId)
    {
        Optional<Restaurant> restaurant = restaurantRepository.findById(restaurantId);
        if (restaurant.isEmpty()) {
            throw new ResourceNotFoundException("Restaurant with id=" + restaurantId + " not exists in database.");
        }


        List<DishDto> result = new ArrayList<>();
        dishRepository.findAllByRestaurant_Id(restaurantId)
                      .forEach(dish -> result.add(DishMapper.getInstance().dtoFromEntity(dish)));

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RestaurantDto> getRestaurantById(Long restaurantId)
    {
        Optional<Restaurant> restaurant = restaurantRepository.findById(restaurantId);
        if (restaurant.isEmpty()) {
            throw new ResourceNotFoundException("Restaurant with id=" + restaurantId + " not exists in database.");
        }


        return new ResponseEntity<>(RestaurantMapper.getInstance().dtoFromEntity(restaurant.get()), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<RestaurantDto>> getRestaurantByName(String restaurantName)
    {
        List<Restaurant> restaurants = restaurantRepository.findAllByNameStartsWith(restaurantName);
        if (restaurants.isEmpty()) {
            throw new ResourceNotFoundException("Restaurant with name=" + restaurantName + " not exists in database.");
        }

        List<RestaurantDto> result = new ArrayList<>();
        restaurants.forEach(restaurant -> result.add(RestaurantMapper.getInstance().dtoFromEntity(restaurant)));

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<RestaurantDto>> getRestaurantList()
    {
        List<Restaurant> restaurants = restaurantRepository.findAll();

        List<RestaurantDto> result = new ArrayList<>();
        restaurants.forEach(restaurant -> result.add(RestaurantMapper.getInstance().dtoFromEntity(restaurant)));

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> removeRestaurant(Long restaurantId) throws Exception
    {
        Optional<Restaurant> restaurant = restaurantRepository.findById(restaurantId);
        if (restaurant.isEmpty()) {
            throw new ResourceNotFoundException("Restaurant with id=" + restaurantId + " not exists in database.");
        }
        // If currently logged in user is not creator of restaurant shouldn't be able to delete or modify.
        if (restaurant.get().getOwnerId() != null &&
                !authenticationHelper.getCurrentLoggedInUser().getId().equals(restaurant.get().getOwnerId())) {
            throw new NotOwnerException(
                    "No access to delete given object. Currently logged in user is not restaurant's owner.");
        }

        restaurantRepository.deleteById(restaurantId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RestaurantDto> updateRestaurant(RestaurantDto restaurantDto) throws Exception
    {
        Optional<Restaurant> restaurant = restaurantRepository.findById(restaurantDto.getId());
        if (restaurant.isEmpty()) {
            throw new ResourceNotFoundException("Restaurant with id=" + restaurantDto.getId()
                                                        + " not exists in database.");
        }
        if (restaurantDto.getAddress().getId() != null &&
                !restaurantDto.getAddress().getId().equals(restaurant.get().getAddress().getId())) {
            throw new BodyHandlingException("It's forbidden to change existing address' id");
        }
        // If currently logged in user is not creator of restaurant shouldn't be able to delete or modify.
        if (restaurant.get().getOwnerId() != null &&
                !authenticationHelper.getCurrentLoggedInUser().getId().equals(restaurant.get().getOwnerId())) {
            throw new NotOwnerException(
                    "No access to update given object. Currently logged in user is not restaurant's owner.");
        }
        try {
            Restaurant updated = RestaurantMapper.getInstance().updateEntityFromDto(restaurantDto, restaurant.get());
            updated = restaurantRepository.save(updated);
            return new ResponseEntity<>(RestaurantMapper.getInstance().dtoFromEntity(updated), HttpStatus.OK);
        } catch (DataIntegrityViolationException | IllegalArgumentException e) {
            throw new BodyHandlingException("Not valid data is given.");
        }
    }
}

