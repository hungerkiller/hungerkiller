package pl.umcs.HungerKiller.service;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.umcs.HungerKiller.exceptions.BodyHandlingException;
import pl.umcs.HungerKiller.exceptions.PasswordNotFitRequirementsException;
import pl.umcs.HungerKiller.mapper.OrderMapper;
import pl.umcs.HungerKiller.mapper.UserMapper;
import pl.umcs.HungerKiller.model.Deliverer;
import pl.umcs.HungerKiller.model.Order;
import pl.umcs.HungerKiller.model.User;
import pl.umcs.HungerKiller.openapi.api.UserApiDelegate;
import pl.umcs.HungerKiller.openapi.model.AuthenticationRequestDto;
import pl.umcs.HungerKiller.openapi.model.AuthenticationResponseDto;
import pl.umcs.HungerKiller.openapi.model.OrderDto;
import pl.umcs.HungerKiller.openapi.model.UserDto;
import pl.umcs.HungerKiller.repository.DelivererRepository;
import pl.umcs.HungerKiller.repository.UserRepository;
import pl.umcs.HungerKiller.security.util.AuthenticationHelper;
import pl.umcs.HungerKiller.security.util.JwtTokenManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserApiDelegate {

    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenManager jwtTokenManager;
    private final AuthenticationHelper authenticationHelper;
    private final PasswordEncoder passwordEncoder;
    private final DelivererRepository delivererRepository;

    @Autowired
    public UserService(UserRepository userRepository,
                       AuthenticationManager authenticationManager,
                       JwtTokenManager jwtTokenManager,
                       AuthenticationHelper authenticationHelper,
                       PasswordEncoder passwordEncoder,
                       DelivererRepository delivererRepository)
    {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
        this.jwtTokenManager = jwtTokenManager;
        this.authenticationHelper = authenticationHelper;
        this.passwordEncoder = passwordEncoder;
        this.delivererRepository = delivererRepository;
    }


    @Override
    public ResponseEntity<AuthenticationResponseDto> loginUser(AuthenticationRequestDto auth)
    {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(auth.getEmail(),
                                                                                       auth.getPassword()));
        } catch (BadCredentialsException e) {
            return ResponseEntity.notFound().build();
        }

        Optional<User> userFromDb = userRepository.getUserByEmail(auth.getEmail());
        if (userFromDb.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        UserDetails userDetails = new org.springframework.security.core.userdetails.User(userFromDb.get().getEmail(),
                                                                                         userFromDb.get().getPassword(),
                                                                                         new ArrayList<>());
        final String jwt = jwtTokenManager.generateToken(userDetails);

        AuthenticationResponseDto result = new AuthenticationResponseDto();
        result.setJwt(jwt);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<Void> deleteUser()
    {
        User user = authenticationHelper.getCurrentLoggedInUser();
        if (user == null) {
            return ResponseEntity.notFound().build();
        }

        if (user.getUserRole() == User.UserRole.urDeliverer) {
            Deliverer deliverer = delivererRepository.findDelivererByUserId(user.getId());
            delivererRepository.delete(deliverer);
        }

        userRepository.delete(user);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<List<OrderDto>> getOrdersForUser()
    {
        List<Order> orders = authenticationHelper.getCurrentLoggedInUser().getOrdersList();

        List<OrderDto> result = new ArrayList<>();
        orders.forEach(order -> result.add(OrderMapper.getInstance().dtoFromEntity(order)));

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UserDto> getUser()
    {
        UserDto userDto = UserMapper.getInstance().dtoFromEntity(authenticationHelper.getCurrentLoggedInUser());
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UserDto> registerUser(UserDto userDto) throws Exception
    {
        if (!EmailValidator.getInstance().isValid(userDto.getEmail())) {
            throw new BodyHandlingException("Provided e-mail is invalid.");
        }

        isValidPassword(userDto.getPassword());

        User user = UserMapper.getInstance().entityFromDto(userDto);
        user.setEnabled('Y');
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        try {
            user = userRepository.save(user);
        } catch (Exception exception) {
            return new ResponseEntity<>(userDto, HttpStatus.BAD_REQUEST);
        }

        if (user.getUserRole() == User.UserRole.urDeliverer) {
            Deliverer deliverer = new Deliverer();
            deliverer.setUserId(user.getId());
            deliverer.setServiceInCity(user.getDeliveryAddress().getTown());
            delivererRepository.save(deliverer);
        }

        return new ResponseEntity<>(UserMapper.getInstance().dtoFromEntity(user), HttpStatus.CREATED);
    }

    public static void isValidPassword(String password)
            throws PasswordNotFitRequirementsException, BodyHandlingException
    {
        if (password == null) {
            throw new BodyHandlingException("Empty password not allowed");
        }

        if (password.length() > 20 || password.length() < 8) {
            throw new PasswordNotFitRequirementsException(
                    "Password must be less than 20 and more than 8 characters in length.");
        }
        String upperCaseChars = "(.*[A-Z].*)";
        if (!password.matches(upperCaseChars)) {
            throw new PasswordNotFitRequirementsException("Password must contain at least one uppercase character");
        }
        String lowerCaseChars = "(.*[a-z].*)";
        if (!password.matches(lowerCaseChars)) {
            throw new PasswordNotFitRequirementsException("Password must contain at least one lowercase character");
        }
        String numbers = "(.*[0-9].*)";
        if (!password.matches(numbers)) {
            throw new PasswordNotFitRequirementsException("Password must contain at least one number");
        }
        String specialChars = "(.*[@#$%!.*\\-+^,_].*$)";
        if (!password.matches(specialChars)) {
            throw new PasswordNotFitRequirementsException(
                    "Password must contain at least one special character among @#$%!.*-+^,_");
        }
    }

    @Override
    public ResponseEntity<UserDto> updateUser(UserDto userDto)
    {
        if (!userDto.getEmail().equals(authenticationHelper.getCurrentLoggedInUser().getEmail())) {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }

        User user = authenticationHelper.getCurrentLoggedInUser();

        User updated = UserMapper.getInstance().updateEntityFromDto(userDto, user);

        updated = userRepository.save(updated);

        return new ResponseEntity<>(UserMapper.getInstance().dtoFromEntity(updated), HttpStatus.OK);
    }
}
