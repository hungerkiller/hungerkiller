package pl.umcs.HungerKiller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import pl.umcs.HungerKiller.openapi.model.AuthenticationRequestDto;
import pl.umcs.HungerKiller.openapi.model.AuthenticationResponseDto;
import pl.umcs.HungerKiller.openapi.model.DelivererDto;
import pl.umcs.HungerKiller.utils.JsonUtils;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class DelivererControllerTest {
    private final MockMvc mvc;

    @Autowired
    public DelivererControllerTest(MockMvc mvc)
    {
        this.mvc = mvc;
    }

    @Test
    void getDelivererInfoWithoutAuthorization() throws Exception
    {
        MvcResult result = mvc.perform(get("/hungerkiller/v1/deliverer"))
                              .andReturn();

        assertEquals(403, result.getResponse().getStatus());
    }

    @Test
    void getDelivererInfoWithAuthorization() throws Exception
    {
        String token = loginUserAndGetToken("AtUserD@test.com", "Testtest2021*");

        MvcResult result = mvc.perform(get("/hungerkiller/v1/deliverer")
                                               .header("Authorization", "Bearer " + token))
                              .andReturn();
        assertEquals(200, result.getResponse().getStatus());
    }

    @Test
    void getDelivererInfoWithAuthorizationButUserIsNotDeliverer() throws Exception
    {
        String token = loginUserAndGetToken("AtUserC@test.com", "Testtest2021*");

        MvcResult result = mvc.perform(get("/hungerkiller/v1/deliverer")
                                               .header("Authorization", "Bearer " + token))
                              .andReturn();
        assertEquals(403, result.getResponse().getStatus());
    }

    @Test
    void getDelivererJobOffersWithoutAuthorization() throws Exception
    {
        MvcResult result = mvc.perform(get("/hungerkiller/v1/deliverer/jobOffers"))
                              .andReturn();

        assertEquals(403, result.getResponse().getStatus());
    }

    @Test
    void getDelivererJobOffersWithAuthorization() throws Exception
    {
        String token = loginUserAndGetToken("AtUserD@test.com", "Testtest2021*");

        MvcResult result = mvc.perform(get("/hungerkiller/v1/deliverer/jobOffers")
                                               .header("Authorization", "Bearer " + token))
                              .andReturn();
        assertEquals(200, result.getResponse().getStatus());
    }


    @Test
    void putDelivererInfoWithoutAuthorization() throws Exception
    {
        DelivererDto delivererDto = new DelivererDto();
        delivererDto.setId(11L);
        delivererDto.setServiceInCity("New Test");

        MvcResult result = mvc.perform(put("/hungerkiller/v1/deliverer/")
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(delivererDto)))
                              .andReturn();

        assertEquals(403, result.getResponse().getStatus());
    }

    @Test
    void putDelivererInfoWithAuthorizationByBadDeliverer() throws Exception
    {
        String token = loginUserAndGetToken("AtUserD@test.com", "Testtest2021*");

        DelivererDto delivererDto = new DelivererDto();
        delivererDto.setId(10L);
        delivererDto.setServiceInCity("New Test");

        MvcResult result = mvc.perform(put("/hungerkiller/v1/deliverer/")
                                               .header("Authorization", "Bearer " + token)
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(delivererDto)))
                              .andReturn();

        assertEquals(403, result.getResponse().getStatus());
    }

    @Test
    void putDelivererInfoWithAuthorizationByGoodDeliverer() throws Exception
    {
        String token = loginUserAndGetToken("AtUserD@test.com", "Testtest2021*");

        DelivererDto delivererDto = new DelivererDto();
        delivererDto.setId(11L);
        delivererDto.setServiceInCity("New Test");

        MvcResult result = mvc.perform(put("/hungerkiller/v1/deliverer/")
                                               .header("Authorization", "Bearer " + token)
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(delivererDto)))
                              .andReturn();

        assertEquals(200, result.getResponse().getStatus());
    }

    private String loginUserAndGetToken(String login, String password) throws Exception
    {
        AuthenticationRequestDto authenticationRequest = new AuthenticationRequestDto();
        authenticationRequest.setEmail(login);
        authenticationRequest.setPassword(password);

        MvcResult result = mvc.perform(post("/hungerkiller/v1/user/login")
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(authenticationRequest)))
                              .andReturn();
        AuthenticationResponseDto response =
                JsonUtils.mapFromJson(result.getResponse().getContentAsString(), AuthenticationResponseDto.class);

        assertNotNull(response.getJwt());
        assertFalse(response.getJwt().isEmpty());
        return response.getJwt();
    }
}

