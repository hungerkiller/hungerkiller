package pl.umcs.HungerKiller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import pl.umcs.HungerKiller.openapi.model.AuthenticationRequestDto;
import pl.umcs.HungerKiller.openapi.model.AuthenticationResponseDto;
import pl.umcs.HungerKiller.openapi.model.DishDto;
import pl.umcs.HungerKiller.openapi.model.RestaurantDto;
import pl.umcs.HungerKiller.utils.JsonUtils;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class DishControllerTest {

    MockMvc mvc;

    @Autowired
    public DishControllerTest(MockMvc mvc)
    {
        this.mvc = mvc;
    }

    @Test
    void getDishList() throws Exception
    {
        MvcResult result = mvc.perform(get("/hungerkiller/v1/dish")).andReturn();

        assertEquals(200, result.getResponse().getStatus());

        DishDto[] dishList = JsonUtils.mapFromJson(result.getResponse().getContentAsString(), DishDto[].class);
        assertTrue(dishList.length > 0);

        for (DishDto dto : dishList) {
            assertNotNull(dto);
        }
    }

    @Test
    void addDishWithoutAuthorization() throws Exception
    {
        DishDto dto = new DishDto();
        dto.setName("AtDish");
        dto.setName("AtDish - added from tests");
        dto.setPrice(55.55);

        RestaurantDto restaurantDto = new RestaurantDto();
        restaurantDto.setId(59L); //AtRestaurant
        dto.setRestaurant(restaurantDto);

        MvcResult result = mvc.perform(post("/hungerkiller/v1/dish")
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(dto)))
                              .andReturn();

        assertEquals(403, result.getResponse().getStatus());
    }

    @Test
    void addDishWhenNotRestaurantOwner() throws Exception
    {
        String token = loginUserAndGetToken("AtUserC@test.com", "Testtest2021*");
        DishDto dto = new DishDto();
        dto.setName("AtDish");
        dto.setDescription("AtDish - added from tests");
        dto.setPrice(55.55);

        RestaurantDto restaurantDto = new RestaurantDto();
        restaurantDto.setId(59L); //AtRestaurant
        dto.setRestaurant(restaurantDto);

        MvcResult result = mvc.perform(post("/hungerkiller/v1/dish")
                                               .header("Authorization", "Bearer " + token)
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(dto)))
                              .andReturn();

        assertEquals(403, result.getResponse().getStatus());
    }

    @Test
    void addDishWhenUserIsRestaurantOwner() throws Exception
    {
        String token = loginUserAndGetToken("AtUserR@test.com", "Testtest2021*");
        DishDto dto = new DishDto();
        dto.setName("AtDish");
        dto.setDescription("AtDish - added from tests");
        dto.setPrice(55.55);

        RestaurantDto restaurantDto = new RestaurantDto();
        restaurantDto.setId(59L); //AtRestaurant
        dto.setRestaurant(restaurantDto);

        MvcResult result = mvc.perform(post("/hungerkiller/v1/dish")
                                               .header("Authorization", "Bearer " + token)
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(dto)))
                              .andReturn();

        assertEquals(201, result.getResponse().getStatus());
        assertFalse(result.getResponse().getContentAsString().isEmpty());

        DishDto addedDish = JsonUtils.mapFromJson(result.getResponse().getContentAsString(), DishDto.class);
        assertNotNull(addedDish.getId());
        assertEquals(59L, addedDish.getRestaurant().getId());
        assertEquals("AtDish", addedDish.getName(), "Name not the same");
        assertEquals("AtDish - added from tests", addedDish.getDescription(), "Desc not the same");
        assertEquals(55.55, addedDish.getPrice(), "Price not the same");
    }

    @Test
    void addDishToNotExistingRestaurant() throws Exception
    {
        String token = loginUserAndGetToken("AtUserR@test.com", "Testtest2021*");
        DishDto dto = new DishDto();
        dto.setName("AtDish");
        dto.setDescription("AtDish - added from tests");
        dto.setPrice(55.55);

        RestaurantDto restaurantDto = new RestaurantDto();
        restaurantDto.setId(9999999999L);
        dto.setRestaurant(restaurantDto);

        MvcResult result = mvc.perform(post("/hungerkiller/v1/dish")
                                               .header("Authorization", "Bearer " + token)
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(dto)))
                              .andReturn();

        assertEquals(404, result.getResponse().getStatus());
    }

    @Test
    void getDishByName() throws Exception
    {
        MvcResult result = mvc.perform(get("/hungerkiller/v1/dish/name/AtDish"))
                              .andReturn();

        assertEquals(200, result.getResponse().getStatus());
        assertFalse(result.getResponse().getContentAsString().isEmpty());

        DishDto[] dishArr = JsonUtils.mapFromJson(result.getResponse().getContentAsString(), DishDto[].class);
        assertTrue(dishArr.length > 0);

        for (DishDto dish : dishArr) {
            assertEquals("AtDish", dish.getName());
        }
    }

    @Test
    void getDishById() throws Exception
    {
        MvcResult result = mvc.perform(get("/hungerkiller/v1/dish/id/104"))
                              .andReturn();

        assertEquals(200, result.getResponse().getStatus());
        assertFalse(result.getResponse().getContentAsString().isEmpty());

        DishDto dish = JsonUtils.mapFromJson(result.getResponse().getContentAsString(), DishDto.class);
        assertEquals(104, dish.getId());
    }

    @Test
    void removeDish() throws Exception
    {
        String token = loginUserAndGetToken("AtUserR@test.com", "Testtest2021*");
        DishDto dto = new DishDto();
        dto.setName("AtDish");
        dto.setDescription("AtDish - added from tests");
        dto.setPrice(55.55);

        RestaurantDto restaurantDto = new RestaurantDto();
        restaurantDto.setId(59L); //AtRestaurant
        dto.setRestaurant(restaurantDto);

        MvcResult result = mvc.perform(post("/hungerkiller/v1/dish")
                                               .header("Authorization", "Bearer " + token)
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(dto)))
                              .andReturn();

        assertEquals(201, result.getResponse().getStatus());
        assertFalse(result.getResponse().getContentAsString().isEmpty());
        DishDto addedDish = JsonUtils.mapFromJson(result.getResponse().getContentAsString(), DishDto.class);

        MvcResult removeResult = mvc.perform(delete("/hungerkiller/v1/dish/id/" + addedDish.getId())
                                                     .header("Authorization", "Bearer " + token))
                                    .andReturn();

        assertEquals(204, removeResult.getResponse().getStatus());
    }

    @Test
    void removeDishWhenNotItsOwner() throws Exception
    {
        String token = loginUserAndGetToken("AtUserR@test.com", "Testtest2021*");
        DishDto dto = new DishDto();
        dto.setName("AtDish");
        dto.setDescription("AtDish - added from tests");
        dto.setPrice(55.55);

        RestaurantDto restaurantDto = new RestaurantDto();
        restaurantDto.setId(59L); //AtRestaurant
        dto.setRestaurant(restaurantDto);

        MvcResult result = mvc.perform(post("/hungerkiller/v1/dish")
                                               .header("Authorization", "Bearer " + token)
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(dto)))
                              .andReturn();

        assertEquals(201, result.getResponse().getStatus());
        assertFalse(result.getResponse().getContentAsString().isEmpty());
        DishDto addedDish = JsonUtils.mapFromJson(result.getResponse().getContentAsString(), DishDto.class);


        token = loginUserAndGetToken("AtUserC@test.com", "Testtest2021*");

        MvcResult removeResult = mvc.perform(delete("/hungerkiller/v1/dish/id/" + addedDish.getId())
                                                     .header("Authorization", "Bearer " + token))
                                    .andReturn();

        assertEquals(403, removeResult.getResponse().getStatus());
    }

    @Test
    void updateDishWhenOwner() throws Exception
    {
        String token = loginUserAndGetToken("AtUserR@test.com", "Testtest2021*");
        DishDto dto = new DishDto();
        dto.setName("AtDish");
        dto.setDescription("AtDish - added from tests");
        dto.setPrice(55.55);

        RestaurantDto restaurantDto = new RestaurantDto();
        restaurantDto.setId(59L); //AtRestaurant
        dto.setRestaurant(restaurantDto);

        MvcResult addResult = mvc.perform(post("/hungerkiller/v1/dish")
                                                  .header("Authorization", "Bearer " + token)
                                                  .contentType(MediaType.APPLICATION_JSON_VALUE)
                                                  .content(JsonUtils.mapToJson(dto)))
                                 .andReturn();

        assertEquals(201, addResult.getResponse().getStatus());
        assertFalse(addResult.getResponse().getContentAsString().isEmpty());
        DishDto addedDish = JsonUtils.mapFromJson(addResult.getResponse().getContentAsString(), DishDto.class);

        addedDish.setPrice(25.78);

        MvcResult updateResult = mvc.perform(put("/hungerkiller/v1/dish")
                                                     .header("Authorization", "Bearer " + token)
                                                     .contentType(MediaType.APPLICATION_JSON_VALUE)
                                                     .content(JsonUtils.mapToJson(addedDish)))
                                    .andReturn();

        assertEquals(200, updateResult.getResponse().getStatus());
        assertFalse(updateResult.getResponse().getContentAsString().isEmpty());

        DishDto updated = JsonUtils.mapFromJson(updateResult.getResponse().getContentAsString(), DishDto.class);
        assertEquals(addedDish.getId(), updated.getId());
        assertEquals(addedDish.getName(), updated.getName());
        assertEquals(addedDish.getDescription(), updated.getDescription());
        assertEquals(25.78, updated.getPrice());
    }

    @Test
    void updateDishWhenNotOwner() throws Exception
    {
        String token = loginUserAndGetToken("AtUserR@test.com", "Testtest2021*");
        DishDto dto = new DishDto();
        dto.setName("AtDish");
        dto.setDescription("AtDish - added from tests");
        dto.setPrice(55.55);

        RestaurantDto restaurantDto = new RestaurantDto();
        restaurantDto.setId(59L); //AtRestaurant
        dto.setRestaurant(restaurantDto);

        MvcResult addResult = mvc.perform(post("/hungerkiller/v1/dish")
                                                  .header("Authorization", "Bearer " + token)
                                                  .contentType(MediaType.APPLICATION_JSON_VALUE)
                                                  .content(JsonUtils.mapToJson(dto)))
                                 .andReturn();

        assertEquals(201, addResult.getResponse().getStatus());
        assertFalse(addResult.getResponse().getContentAsString().isEmpty());
        DishDto addedDish = JsonUtils.mapFromJson(addResult.getResponse().getContentAsString(), DishDto.class);

        addedDish.setPrice(25.78);

        token = loginUserAndGetToken("AtUserC@test.com", "Testtest2021*");
        MvcResult updateResult = mvc.perform(put("/hungerkiller/v1/dish")
                                                     .header("Authorization", "Bearer " + token)
                                                     .contentType(MediaType.APPLICATION_JSON_VALUE)
                                                     .content(JsonUtils.mapToJson(addedDish)))
                                    .andReturn();

        assertEquals(403, updateResult.getResponse().getStatus());
    }

    private String loginUserAndGetToken(final String email, final String password) throws Exception
    {
        AuthenticationRequestDto authenticationRequest = new AuthenticationRequestDto();
        authenticationRequest.setEmail(email);
        authenticationRequest.setPassword(password);

        MvcResult result = mvc.perform(post("/hungerkiller/v1/user/login")
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(authenticationRequest)))
                              .andReturn();
        AuthenticationResponseDto response =
                JsonUtils.mapFromJson(result.getResponse().getContentAsString(), AuthenticationResponseDto.class);

        return response.getJwt();
    }
}