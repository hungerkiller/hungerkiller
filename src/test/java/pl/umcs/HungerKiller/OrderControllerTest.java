package pl.umcs.HungerKiller;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import pl.umcs.HungerKiller.openapi.model.*;
import pl.umcs.HungerKiller.utils.JsonUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class OrderControllerTest {
    private final MockMvc mvc;

    @Autowired
    public OrderControllerTest(MockMvc mvc)
    {
        this.mvc = mvc;
    }

    @Test
    void getOrdersWithoutAuthorization() throws Exception
    {
        MvcResult result = mvc.perform(get("/hungerkiller/v1/order"))
                              .andReturn();

        assertEquals(403, result.getResponse().getStatus());
    }

    @Test
    void getOrdersWithAuthorization() throws Exception
    {
        String token = loginUserAndGetToken("AtUserC@test.com", "Testtest2021*");


        MvcResult result = mvc.perform(get("/hungerkiller/v1/order")
                                               .header("Authorization", "Bearer " + token))
                              .andReturn();
        assertEquals(200, result.getResponse().getStatus());
    }

    @Test
    void addOrderWithoutAuthorization() throws Exception
    {
        OrderDto newOrder = new OrderDto();
        RestaurantDto restaurantDto = new RestaurantDto();
        restaurantDto.setId(2L);
        List<DishDto> dishes = new ArrayList<>();
        newOrder.setRestaurant(restaurantDto);
        newOrder.setDishes(dishes);

        MvcResult result = mvc.perform(post("/hungerkiller/v1/order")
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(newOrder)))
                              .andReturn();


        assertEquals(403, result.getResponse().getStatus());
    }

    @Test
    void addOrderWithAuthorizationByCustomer() throws Exception
    {
        String token = loginUserAndGetToken("AtUserC@test.com", "Testtest2021*");

        OrderDto newOrder = new OrderDto();
        RestaurantDto restaurantDto = new RestaurantDto();
        restaurantDto.setId(2L);
        List<DishDto> dishes = new ArrayList<>();
        newOrder.setRestaurant(restaurantDto);
        newOrder.setDishes(dishes);


        MvcResult result = mvc.perform(post("/hungerkiller/v1/order")
                                               .header("Authorization", "Bearer " + token)
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(newOrder)))
                              .andReturn();


        assertEquals(201, result.getResponse().getStatus());
    }

    @Test
    void addOrderWithAuthorizationByDeliverer() throws Exception
    {
        String token = loginUserAndGetToken("AtUserD@test.com", "Testtest2021*");

        OrderDto newOrder = new OrderDto();
        RestaurantDto restaurantDto = new RestaurantDto();
        restaurantDto.setId(2L);
        List<DishDto> dishes = new ArrayList<>();
        newOrder.setRestaurant(restaurantDto);
        newOrder.setDishes(dishes);


        MvcResult result = mvc.perform(post("/hungerkiller/v1/order")
                                               .header("Authorization", "Bearer " + token)
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(newOrder)))
                              .andReturn();


        assertEquals(403, result.getResponse().getStatus());
    }

    @Test
    void addOrderWithAuthorizationByRestaurateur() throws Exception
    {
        String token = loginUserAndGetToken("AtUserR@test.com", "Testtest2021*");

        OrderDto newOrder = new OrderDto();
        RestaurantDto restaurantDto = new RestaurantDto();
        restaurantDto.setId(2L);
        List<DishDto> dishes = new ArrayList<>();
        newOrder.setRestaurant(restaurantDto);
        newOrder.setDishes(dishes);


        MvcResult result = mvc.perform(post("/hungerkiller/v1/order")
                                               .header("Authorization", "Bearer " + token)
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(newOrder)))
                              .andReturn();


        assertEquals(403, result.getResponse().getStatus());
    }

    @Test
    void updateOrderWithoutAuthorization() throws Exception
    {
        OrderDto orderDto = new OrderDto();
        orderDto.setDelivererId(8L);
        orderDto.setId(38L);
        orderDto.setStatus(OrderDto.StatusEnum.DONE);

        MvcResult result = mvc.perform(put("/hungerkiller/v1/order")
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(orderDto)))
                              .andReturn();


        assertEquals(403, result.getResponse().getStatus());
    }

    @Test
    void updateOrderWithAuthorizationByCustomer() throws Exception
    {
        String token = loginUserAndGetToken("AtUserC@test.com", "Testtest2021*");

        OrderDto orderDto = new OrderDto();
        orderDto.setDelivererId(8L);
        orderDto.setId(38L);
        orderDto.setStatus(OrderDto.StatusEnum.DONE);

        MvcResult result = mvc.perform(put("/hungerkiller/v1/order")
                                               .header("Authorization", "Bearer " + token)
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(orderDto)))
                              .andReturn();


        assertEquals(403, result.getResponse().getStatus());
    }

    @Test
    void updateOrderWithAuthorizationByRestaurateur() throws Exception
    {
        String token = loginUserAndGetToken("AtUserR@test.com", "Testtest2021*");

        OrderDto orderDto = new OrderDto();
        orderDto.setDelivererId(8L);
        orderDto.setId(38L);
        orderDto.setStatus(OrderDto.StatusEnum.DONE);

        MvcResult result = mvc.perform(put("/hungerkiller/v1/order")
                                               .header("Authorization", "Bearer " + token)
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(orderDto)))
                              .andReturn();


        assertEquals(403, result.getResponse().getStatus());
    }

    @Test
    void updateOrderWithAuthorizationByBadDeliverer() throws Exception
    {
        String token = loginUserAndGetToken("AtUserD@test.com", "Testtest2021*");

        OrderDto orderDto = new OrderDto();
        orderDto.setDelivererId(8L);
        orderDto.setId(38L);
        orderDto.setStatus(OrderDto.StatusEnum.DONE);

        MvcResult result = mvc.perform(put("/hungerkiller/v1/order")
                                               .header("Authorization", "Bearer " + token)
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(orderDto)))
                              .andReturn();


        assertEquals(403, result.getResponse().getStatus());
    }

    @Test
    void updateOrderWithAuthorizationByGoodDeliverer() throws Exception
    {
        String token = loginUserAndGetToken("apollddo@wp.pl", "Pzn1919");

        OrderDto orderDto = new OrderDto();
        orderDto.setDelivererId(8L);
        orderDto.setId(38L);
        orderDto.setStatus(OrderDto.StatusEnum.DONE);


        MvcResult result = mvc.perform(put("/hungerkiller/v1/order")
                                               .header("Authorization", "Bearer " + token)
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(orderDto)))
                              .andReturn();


        assertEquals(200, result.getResponse().getStatus());
    }

    private String loginUserAndGetToken(String login, String password) throws Exception
    {
        AuthenticationRequestDto authenticationRequest = new AuthenticationRequestDto();
        authenticationRequest.setEmail(login);
        authenticationRequest.setPassword(password);

        MvcResult result = mvc.perform(post("/hungerkiller/v1/user/login")
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(authenticationRequest)))
                              .andReturn();
        AuthenticationResponseDto response =
                JsonUtils.mapFromJson(result.getResponse().getContentAsString(), AuthenticationResponseDto.class);

        assertNotNull(response.getJwt());
        assertFalse(response.getJwt().isEmpty());
        return response.getJwt();
    }


}