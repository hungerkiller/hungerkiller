package pl.umcs.HungerKiller;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import pl.umcs.HungerKiller.openapi.model.AddressDto;
import pl.umcs.HungerKiller.openapi.model.AuthenticationRequestDto;
import pl.umcs.HungerKiller.openapi.model.AuthenticationResponseDto;
import pl.umcs.HungerKiller.openapi.model.UserDto;
import pl.umcs.HungerKiller.repository.DelivererRepository;
import pl.umcs.HungerKiller.utils.JsonUtils;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class UserControllerTest {

    private final MockMvc mvc;
    private final DelivererRepository delivererRepository;

    @Autowired
    public UserControllerTest(MockMvc mvc, DelivererRepository delivererRepository)
    {
        this.mvc = mvc;
        this.delivererRepository = delivererRepository;
    }

    @Test
    void registerUserWithAnInvalidEmail() throws Exception
    {
        UserDto newUser = new UserDto();
        newUser.setEmail("example@example");

        MvcResult result = mvc.perform(post("/hungerkiller/v1/user")
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(newUser)))
                              .andReturn();

        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    void registerUserWithAnEmptyPassword() throws Exception
    {
        UserDto newUser = new UserDto();
        newUser.setEmail("example@example.pl");

        MvcResult result = mvc.perform(post("/hungerkiller/v1/user")
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(newUser)))
                              .andReturn();

        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    void registerUserWithTooShortPassword() throws Exception
    {
        UserDto newUser = new UserDto();
        newUser.setEmail("example@example.pl");
        newUser.setPassword("1234567");

        MvcResult result = mvc.perform(post("/hungerkiller/v1/user")
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(newUser)))
                              .andReturn();

        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    void registerUserWithPasswordWithoutNumber() throws Exception
    {
        UserDto newUser = new UserDto();
        newUser.setEmail("example@example.pl");
        newUser.setPassword("abcdefghi");

        MvcResult result = mvc.perform(post("/hungerkiller/v1/user")
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(newUser)))
                              .andReturn();

        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    void registerUserWithPasswordWithoutSpecialCharacter() throws Exception
    {
        UserDto newUser = new UserDto();
        newUser.setEmail("example@example.pl");
        newUser.setPassword("abcdefghi2");

        MvcResult result = mvc.perform(post("/hungerkiller/v1/user")
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(newUser)))
                              .andReturn();

        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    @Order(1)
    void registerUser() throws Exception
    {
        UserDto newUser = new UserDto();
        newUser.setEmail("example@example.pl");
        newUser.setPassword("aBcdefghi2*");
        AddressDto addressDto = new AddressDto();
        addressDto.setTown("test");
        addressDto.setZipcode("test");
        addressDto.setStreet("test");
        newUser.setDeliveryAddress(addressDto);
        newUser.setName("Tester");
        newUser.setSurname("Tester");
        newUser.setRole(UserDto.RoleEnum.CUSTOMER);

        MvcResult result = mvc.perform(post("/hungerkiller/v1/user")
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(newUser)))
                              .andReturn();

        assertEquals(201, result.getResponse().getStatus());
    }

    @Test
    void loginUser() throws Exception
    {
        AuthenticationRequestDto authenticationRequest = new AuthenticationRequestDto();
        authenticationRequest.setEmail("AtUserC@test.com");
        authenticationRequest.setPassword("Testtest2021*");

        MvcResult result = mvc.perform(post("/hungerkiller/v1/user/login")
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(authenticationRequest)))
                              .andReturn();

        assertEquals(200, result.getResponse().getStatus());

        AuthenticationResponseDto response =
                JsonUtils.mapFromJson(result.getResponse().getContentAsString(), AuthenticationResponseDto.class);

        assertNotNull(response);
        assertFalse(response.getJwt().isEmpty());
    }

    @Test
    void getUserWhenAuthorized() throws Exception
    {
        String token = loginUserAndGetToken();

        MvcResult result = mvc.perform(get("/hungerkiller/v1/user")
                                               .header("Authorization", "Bearer " + token))
                              .andReturn();
        assertEquals(200, result.getResponse().getStatus());

        UserDto user = JsonUtils.mapFromJson(result.getResponse().getContentAsString(), UserDto.class);

        assertEquals("AtUserC@test.com", user.getEmail());
    }

    @Test
    @Order(2)
    void updateUserWhenAuthorized() throws Exception
    {
        AuthenticationRequestDto authenticationRequest = new AuthenticationRequestDto();
        authenticationRequest.setEmail("example@example.pl");
        authenticationRequest.setPassword("aBcdefghi2*");

        MvcResult loginResult = mvc.perform(post("/hungerkiller/v1/user/login")
                                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                                    .content(JsonUtils.mapToJson(authenticationRequest)))
                                   .andReturn();
        assertEquals(200, loginResult.getResponse().getStatus());

        AuthenticationResponseDto response =
                JsonUtils.mapFromJson(loginResult.getResponse().getContentAsString(), AuthenticationResponseDto.class);
        String token = response.getJwt();

        MvcResult result = mvc.perform(get("/hungerkiller/v1/user")
                                               .header("Authorization", "Bearer " + token))
                              .andReturn();
        assertEquals(200, result.getResponse().getStatus());

        UserDto currentUser = JsonUtils.mapFromJson(result.getResponse().getContentAsString(), UserDto.class);
        currentUser.setName("Changed");

        MvcResult updateResult = mvc.perform(put("/hungerkiller/v1/user")
                                                     .header("Authorization", "Bearer " + token)
                                                     .contentType(MediaType.APPLICATION_JSON_VALUE)
                                                     .content(JsonUtils.mapToJson(currentUser)))
                                    .andReturn();
        assertEquals(200, updateResult.getResponse().getStatus());

        UserDto changedUser = JsonUtils.mapFromJson(updateResult.getResponse().getContentAsString(), UserDto.class);
        assertEquals(currentUser.getId(), changedUser.getId());
        assertEquals("Changed", changedUser.getName());
    }

    @Test
    @Order(3)
    void deleteUserWhenAuthorized() throws Exception
    {
        // FIXME: This is code replication but necessary to avoid deleting ATuser. Need to be another way...
        AuthenticationRequestDto authenticationRequest = new AuthenticationRequestDto();
        authenticationRequest.setEmail("example@example.pl");
        authenticationRequest.setPassword("aBcdefghi2*");

        MvcResult loginResult = mvc.perform(post("/hungerkiller/v1/user/login")
                                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                                    .content(JsonUtils.mapToJson(authenticationRequest)))
                                   .andReturn();
        AuthenticationResponseDto response =
                JsonUtils.mapFromJson(loginResult.getResponse().getContentAsString(), AuthenticationResponseDto.class);
        String token = response.getJwt();

        MvcResult result = mvc.perform(delete("/hungerkiller/v1/user")
                                               .header("Authorization", "Bearer " + token))
                              .andReturn();

        assertEquals(204, result.getResponse().getStatus());
    }

    @Test
    @Order(3)
    void registerUserWithDelivererRole() throws Exception
    {
        UserDto newUser = new UserDto();
        newUser.setEmail("exampleD@example.pl");
        newUser.setPassword("aBcdefghi2*");
        newUser.setRole(UserDto.RoleEnum.DELIVERER);
        AddressDto addressDto = new AddressDto();
        addressDto.setTown("test");
        addressDto.setZipcode("test");
        addressDto.setStreet("test");
        newUser.setDeliveryAddress(addressDto);
        newUser.setName("Tester");
        newUser.setSurname("Tester");
        newUser.setRole(UserDto.RoleEnum.DELIVERER);

        MvcResult result = mvc.perform(post("/hungerkiller/v1/user")
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(newUser)))
                              .andReturn();

        assertEquals(201, result.getResponse().getStatus());
        Long idU = JsonUtils.mapFromJson(result.getResponse().getContentAsString(), UserDto.class).getId();

        assertNotNull(delivererRepository.findDelivererByUserId(idU));
    }

    @Test
    @Order(4)
    void deleteUserWithDelivererRoleWhenAuthorized() throws Exception
    {
        AuthenticationRequestDto authenticationRequest = new AuthenticationRequestDto();
        authenticationRequest.setEmail("exampleD@example.pl");
        authenticationRequest.setPassword("aBcdefghi2*");

        MvcResult loginResult = mvc.perform(post("/hungerkiller/v1/user/login")
                                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                                    .content(JsonUtils.mapToJson(authenticationRequest)))
                                   .andReturn();
        AuthenticationResponseDto response =
                JsonUtils.mapFromJson(loginResult.getResponse().getContentAsString(), AuthenticationResponseDto.class);
        String token = response.getJwt();

        MvcResult result = mvc.perform(delete("/hungerkiller/v1/user")
                                               .header("Authorization", "Bearer " + token))
                              .andReturn();

        assertEquals(204, result.getResponse().getStatus());
    }


    @Test
    void deleteUserWithoutAuthorization() throws Exception
    {
        MvcResult result = mvc.perform(delete("/hungerkiller/v1/user"))
                              .andReturn();

        assertEquals(403, result.getResponse().getStatus());
    }

    @Test
    void getUserWithoutAuthorization() throws Exception
    {
        MvcResult result = mvc.perform(get("/hungerkiller/v1/user"))
                              .andReturn();

        assertEquals(403, result.getResponse().getStatus());
    }

    @Test
    void updateUserWithoutAuthorization() throws Exception
    {
        UserDto userToUpdate = new UserDto();
        userToUpdate.setSurname("Changed");
        userToUpdate.setName("Changed");
        userToUpdate.setId(73L); // AtUserC id

        MvcResult result = mvc.perform(put("/hungerkiller/v1/user")
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(userToUpdate)))
                              .andReturn();

        assertEquals(403, result.getResponse().getStatus());
    }

    private String loginUserAndGetToken() throws Exception
    {
        AuthenticationRequestDto authenticationRequest = new AuthenticationRequestDto();
        authenticationRequest.setEmail("AtUserC@test.com");
        authenticationRequest.setPassword("Testtest2021*");

        MvcResult result = mvc.perform(post("/hungerkiller/v1/user/login")
                                               .contentType(MediaType.APPLICATION_JSON_VALUE)
                                               .content(JsonUtils.mapToJson(authenticationRequest)))
                              .andReturn();
        AuthenticationResponseDto response =
                JsonUtils.mapFromJson(result.getResponse().getContentAsString(), AuthenticationResponseDto.class);

        assertNotNull(response.getJwt());
        assertFalse(response.getJwt().isEmpty());
        return response.getJwt();
    }

}