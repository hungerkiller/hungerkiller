package pl.umcs.HungerKiller.controller;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import pl.umcs.HungerKiller.mapper.AddressMapper;
import pl.umcs.HungerKiller.model.Dish;
import pl.umcs.HungerKiller.model.Restaurant;
import pl.umcs.HungerKiller.model.User;
import pl.umcs.HungerKiller.openapi.model.*;
import pl.umcs.HungerKiller.repository.AddressRepository;
import pl.umcs.HungerKiller.repository.DishRepository;
import pl.umcs.HungerKiller.repository.RestaurantRepository;
import pl.umcs.HungerKiller.repository.UserRepository;
import pl.umcs.HungerKiller.utils.JsonUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;


@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class RestaurantControllerTest {

    private final MockMvc mvc;
    RestaurantRepository restaurantRepository;
    DishRepository dishRepository;
    UserRepository userRepository;
    AddressRepository addressRepository;
    String clientToken;
    String restaurateurToken;

    static String testName = "Test_Test_name46272452834655";

    @Autowired
    public RestaurantControllerTest(MockMvc mvc,
                                    RestaurantRepository restaurantRepository,
                                    DishRepository dishRepository,
                                    UserRepository userRepository,
                                    AddressRepository addressRepository) throws Exception
    {
        this.mvc = mvc;
        this.restaurantRepository = restaurantRepository;
        this.dishRepository = dishRepository;
        this.userRepository = userRepository;
        this.addressRepository = addressRepository;


        AuthenticationRequestDto authenticationRequest = new AuthenticationRequestDto();
        authenticationRequest.setEmail("AtUserC@test.com");
        authenticationRequest.setPassword("Testtest2021*");

        MvcResult loginResult = mvc.perform(post("/hungerkiller/v1/user/login")
                                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                                    .content(JsonUtils.mapToJson(authenticationRequest)))
                                   .andReturn();
        AuthenticationResponseDto response =
                JsonUtils.mapFromJson(loginResult.getResponse().getContentAsString(), AuthenticationResponseDto.class);
        this.clientToken = response.getJwt();


        authenticationRequest = new AuthenticationRequestDto();
        authenticationRequest.setEmail("AtUserR@test.com");
        authenticationRequest.setPassword("Testtest2021*");

        loginResult = mvc.perform(post("/hungerkiller/v1/user/login")
                                          .contentType(MediaType.APPLICATION_JSON_VALUE)
                                          .content(JsonUtils.mapToJson(authenticationRequest)))
                         .andReturn();
        response =
                JsonUtils.mapFromJson(loginResult.getResponse().getContentAsString(), AuthenticationResponseDto.class);
        this.restaurateurToken = response.getJwt();

        System.out.println(this.clientToken);
        System.out.println(this.restaurateurToken);
    }


    private static String shuffleString(String string)
    {
        List<String> letters = Arrays.asList(string.split(""));
        Collections.shuffle(letters);
        StringBuilder shuffled = new StringBuilder();
        for (String letter : letters) {
            shuffled.append(letter);
        }
        return shuffled.toString();
    }

    private static RestaurantDto getRestaurantDtoForUpdate(Restaurant restaurant)
    {
        AddressDto addressDto = AddressMapper.getInstance().dtoFromEntity(restaurant.getAddress());
        addressDto.setStreet(shuffleString(addressDto.getStreet()));
        addressDto.setTown(shuffleString(addressDto.getTown()));
        addressDto.setZipcode(shuffleString(addressDto.getZipcode()));

        RestaurantDto restaurantDto = new RestaurantDto();
        restaurantDto.setId(restaurant.getId());
        restaurantDto.setName(shuffleString(restaurant.getName()));
        restaurantDto.setAddress(addressDto);
        restaurantDto.setDescription(shuffleString(restaurant.getDescription()));

        String fromTime = restaurant.getOpenFrom().toLocalTime().plusSeconds(1).toString();
        String tillTime = restaurant.getOpenTill().toLocalTime().plusSeconds(1).toString();

        if (fromTime.length() != 8) {
            fromTime = restaurant.getOpenFrom().toLocalTime().plusSeconds(2).toString();
        }

        if (tillTime.length() != 8) {
            tillTime = restaurant.getOpenTill().toLocalTime().plusSeconds(2).toString();
        }


        restaurantDto.setOpenFromTime(fromTime);
        restaurantDto.setOpenTillTime(tillTime);

        restaurantDto.setPhoneNumber(shuffleString(restaurant.getPhoneNumber()));
        return restaurantDto;
    }

    private static RestaurantDto getRestaurantDtoForAdding()
    {
        String street = "Street";
        String town = "Town";
        String zipcode = "123456";
        String name = RestaurantControllerTest.testName;
        String description = "description";
        String fromTime = "11:00:00";
        String tillTIme = "11:00:00";
        String phoneNumber = "213456789";


        AddressDto addressDto = new AddressDto();
        addressDto.setStreet(street);
        addressDto.setTown(town);
        addressDto.setZipcode(zipcode);

        RestaurantDto restaurantDto = new RestaurantDto();
        restaurantDto.setName(name);
        restaurantDto.setAddress(addressDto);
        restaurantDto.setDescription(description);
        restaurantDto.setOpenFromTime(fromTime);
        restaurantDto.setOpenTillTime(tillTIme);
        restaurantDto.setPhoneNumber(phoneNumber);
        return restaurantDto;
    }
    // Get restaurant by ID

    @Test
    void getRestaurantById() throws Exception
    {
        Optional<Restaurant> restaurants = restaurantRepository.findFirstByNameStartingWith("");

        assertFalse(restaurants.isEmpty(), "At list one restaurant in dataBase needed.");

        Restaurant testRestaurant = restaurants.get();

        MvcResult result = mvc.perform(get("/hungerkiller/v1/restaurant/id/" + testRestaurant.getId().toString()))
                              .andReturn();

        RestaurantDto responseRestaurant =
                JsonUtils.mapFromJson(result.getResponse().getContentAsString(), RestaurantDto.class);

        assertEquals(200, result.getResponse().getStatus(), "Invalid status, should be 200");
        assertEquals(testRestaurant.getId(), responseRestaurant.getId(), "Not valid response");
    }

    @Test
    void getRestaurantByNotExistingId() throws Exception
    {
        MvcResult result = mvc.perform(get("/hungerkiller/v1/restaurant/id/" + "2147483648"))
                              .andReturn();

        assertEquals(404, result.getResponse().getStatus(), "Invalid status, should be 404");
    }

    @Test
    void getRestaurantByNotValidId() throws Exception
    {
        MvcResult result = mvc.perform(get("/hungerkiller/v1/restaurant/id/" + "id"))
                              .andReturn();


        assertEquals(400, result.getResponse().getStatus(), "Invalid status, should be 404");
    }


    // Get dishes in restaurant by ID

    @Test
    void getDishesByRestaurantId() throws Exception
    {
        Optional<Dish> dish = dishRepository.findFirstByNameStartingWith("");

        assertFalse(dish.isEmpty(), "At list one dish in dataBase needed.");

        Optional<Restaurant> restaurant = restaurantRepository.findById(dish.get().getRestaurant().getId());

        assertFalse(restaurant.isEmpty(), "DatBase problem.");

        Long restId = restaurant.get().getId();

        MvcResult result = mvc.perform(get("/hungerkiller/v1/restaurant/id/" + restId.toString() + "/dish"))
                              .andReturn();

        DishDto[] resultDishes = JsonUtils.mapFromLIst(result.getResponse().getContentAsString(), DishDto[].class);

        boolean assertion = false;
        for (DishDto d : resultDishes) {
            System.out.println(d.getId().getClass().getSimpleName());
            System.out.println(dish.get().getId().getClass().getSimpleName());
            if (d.getId().toString().equals(dish.get().getId().toString())) {
                assertion = true;
                break;
            }
        }

        assertEquals(200, result.getResponse().getStatus());
        assertTrue(assertion);
    }

    @Test
    void getDishesByRestaurantNotExistingId() throws Exception
    {
        MvcResult result = mvc.perform(get("/hungerkiller/v1/restaurant/id/" + "2147483648" + "/dish"))
                              .andReturn();

        assertEquals(404, result.getResponse().getStatus());
    }

    // Get list of restaurants

    @Test
    void getLIstOfRestaurants() throws Exception
    {
        List<Restaurant> restaurants = restaurantRepository.findAll();

        MvcResult result = mvc.perform(get("/hungerkiller/v1/restaurant"))
                              .andReturn();


        RestaurantDto[] resultRestaurants =
                JsonUtils.mapFromLIst(result.getResponse().getContentAsString(), RestaurantDto[].class);

        assertEquals(200, result.getResponse().getStatus());
        assertEquals(restaurants.size(), resultRestaurants.length);

    }

    // Get restaurant by name

    @Test
    void getRestaurantByName() throws Exception
    {
        Optional<Restaurant> restaurants = restaurantRepository.findFirstByNameStartingWith("");

        assertFalse(restaurants.isEmpty(), "At list one restaurant in dataBase needed.");

        Restaurant testRestaurant = restaurants.get();

        MvcResult result = mvc.perform(get("/hungerkiller/v1/restaurant/name/" + testRestaurant.getName()))
                              .andReturn();

        RestaurantDto[] resultRestaurants =
                JsonUtils.mapFromLIst(result.getResponse().getContentAsString(), RestaurantDto[].class);

        boolean assertion = false;
        for (RestaurantDto d : resultRestaurants) {
            if (d.getId().toString().equals(restaurants.get().getId().toString())) {
                assertion = true;
                break;
            }
        }

        assertEquals(200, result.getResponse().getStatus());
        assertTrue(assertion);

    }

    @Test
    void getRestaurantByNotExistingName() throws Exception
    {
        MvcResult result = mvc.perform(get("/hungerkiller/v1/restaurant/name/" + shuffleString("HelloWorld")))
                              .andReturn();

        assertEquals(404, result.getResponse().getStatus());
    }

    // Add new dish for restaurant

    @Test
    void addNewDishForRestaurant() throws Exception
    {
        Optional<User> user = userRepository.getUserByEmail("AtUserR@test.com");
        assertFalse(user.isEmpty(), "User doesn't exist.");

        Optional<Restaurant> restaurant = restaurantRepository
                .findFirstByOwnerIdAndName(user.get().getId(), RestaurantControllerTest.testName);

        assertFalse(restaurant.isEmpty(),
                    "Test user hasn't restaurants, name of restaurant must be: " + RestaurantControllerTest.testName + ".");


        DishDto newDish = new DishDto();
        newDish.setName("name");
        newDish.setDescription("description");
        newDish.setPrice(25D);

        MvcResult result =
                mvc.perform(post("/hungerkiller/v1/restaurant/id/" + restaurant.get().getId().toString() + "/dish")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(newDish))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        DishDto responseDish = JsonUtils.mapFromJson(result.getResponse().getContentAsString(), DishDto.class);

        assertEquals(responseDish.getName(), newDish.getName());
        assertEquals(201, result.getResponse().getStatus());
    }

    @Test
    void addNewDishForNotExistingRestaurant() throws Exception
    {
        DishDto newDish = new DishDto();
        newDish.setName("name");
        newDish.setDescription("description");
        newDish.setPrice(25D);

        MvcResult result =
                mvc.perform(post("/hungerkiller/v1/restaurant/id/" + "2147483648" + "/dish")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(newDish))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();
        System.out.println(result.getResponse().getStatus());
        assertEquals(404, result.getResponse().getStatus());
    }

    @Test
    void addNewDishForUnLodgedUser() throws Exception
    {
        Optional<User> user = userRepository.getUserByEmail("AtUserR@test.com");

        assertFalse(user.isEmpty(), "User doesn't exist.");

        Optional<Restaurant> restaurant = restaurantRepository
                .findFirstByOwnerIdAndName(user.get().getId(),
                                           RestaurantControllerTest.testName);

        assertFalse(restaurant.isEmpty(),
                    "Test user hasn't restaurants, name of restaurant must be:" + RestaurantControllerTest.testName + ".");

        DishDto newDish = new DishDto();
        newDish.setName("name");
        newDish.setDescription("description");
        newDish.setPrice(25D);

        MvcResult result =
                mvc.perform(post("/hungerkiller/v1/restaurant/id/" + restaurant.get().getId() + "/dish")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(newDish)))
                   .andReturn();
        assertEquals(403, result.getResponse().getStatus());
    }

    @Test
    void addNewDishForNotOwnerUser() throws Exception
    {
        Optional<User> user = userRepository.getUserByEmail("AtUserR@test.com");

        assertFalse(user.isEmpty(), "User doesn't exist.");

        Optional<Restaurant> restaurant = restaurantRepository
                .findFirstByOwnerIdAndName(user.get().getId(),
                                           RestaurantControllerTest.testName);

        assertFalse(restaurant.isEmpty(),
                    "Test user hasn't restaurants, name of restaurant must be:" + RestaurantControllerTest.testName + ".");

        DishDto newDish = new DishDto();
        newDish.setName("name");
        newDish.setDescription("description");
        newDish.setPrice(25D);

        MvcResult result =
                mvc.perform(post("/hungerkiller/v1/restaurant/id/" + restaurant.get().getId().toString() + "/dish")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(newDish))
                                    .header("Authorization", "Bearer " + this.clientToken))
                   .andReturn();

        assertEquals(403, result.getResponse().getStatus());
    }

    // Add new restaurant

    @Test
    void addRestaurantWithId() throws Exception
    {
        AddressDto addressDto = new AddressDto();
        addressDto.setStreet("Street");
        addressDto.setTown("Town");
        addressDto.setZipcode("123456");

        RestaurantDto restaurantDto = new RestaurantDto();
        restaurantDto.setId(1L);
        restaurantDto.setName("name");
        restaurantDto.setAddress(addressDto);
        restaurantDto.setDescription("description");
        restaurantDto.setOpenFromTime("11:00:00");
        restaurantDto.setOpenTillTime("11:00:00");
        restaurantDto.setPhoneNumber("1234556788");

        MvcResult result =
                mvc.perform(post("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        assertEquals(result.getResponse().getContentAsString(),
                     "Given Restaurant object got id. It could override exiting one. Aborted.");
        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    void addRestaurantUnLoggedUser() throws Exception
    {
        RestaurantDto restaurantDto = getRestaurantDtoForAdding();

        MvcResult result =
                mvc.perform(post("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.clientToken))
                   .andReturn();

        assertEquals(403, result.getResponse().getStatus());
    }

    @Test
    void addRestaurantNotRestaurateur() throws Exception
    {
        RestaurantDto restaurantDto = getRestaurantDtoForAdding();

        MvcResult result =
                mvc.perform(post("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto)))
                   .andReturn();

        assertEquals(403, result.getResponse().getStatus());
    }

    @Test
    void addRestaurantWithNotValidName() throws Exception
    {
        RestaurantDto restaurantDto = getRestaurantDtoForAdding();
        restaurantDto.setName("n".repeat(51));

        MvcResult result =
                mvc.perform(post("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        assertEquals(result.getResponse().getContentAsString(), "Not valid data is given.");
        assertEquals(400, result.getResponse().getStatus());

    }

    @Test
    void addRestaurantWithNotValidStreet() throws Exception
    {
        RestaurantDto restaurantDto = getRestaurantDtoForAdding();
        restaurantDto.getAddress().setStreet("s".repeat(51));

        MvcResult result =
                mvc.perform(post("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        assertEquals(result.getResponse().getContentAsString(), "Not valid data is given.");
        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    void addRestaurantWithNotValidTown() throws Exception
    {
        RestaurantDto restaurantDto = getRestaurantDtoForAdding();
        restaurantDto.getAddress().setTown("t".repeat(51));

        MvcResult result =
                mvc.perform(post("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        assertEquals(result.getResponse().getContentAsString(), "Not valid data is given.");
        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    void addRestaurantWithNotValidZipcode() throws Exception
    {
        RestaurantDto restaurantDto = getRestaurantDtoForAdding();
        restaurantDto.getAddress().setZipcode("z".repeat(7));

        MvcResult result =
                mvc.perform(post("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        assertEquals(result.getResponse().getContentAsString(), "Not valid data is given.");
        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    void addRestaurantWithNotValidDescription() throws Exception
    {
        RestaurantDto restaurantDto = getRestaurantDtoForAdding();
        restaurantDto.setDescription("d".repeat(201));

        MvcResult result =
                mvc.perform(post("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        assertEquals(result.getResponse().getContentAsString(), "Not valid data is given.");
        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    void addRestaurantWithNotValidFromTime() throws Exception
    {
        RestaurantDto restaurantDto = getRestaurantDtoForAdding();
        restaurantDto.setOpenFromTime("00:11:1f");

        MvcResult result =
                mvc.perform(post("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        assertEquals(result.getResponse().getContentAsString(), "Not valid data is given.");
        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    void addRestaurantWithNotValidTillTime() throws Exception
    {
        RestaurantDto restaurantDto = getRestaurantDtoForAdding();
        restaurantDto.setOpenTillTime("00:11:1f");

        MvcResult result =
                mvc.perform(post("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        assertEquals(result.getResponse().getContentAsString(), "Not valid data is given.");
        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    void addRestaurantWithNotValidPhoneNumber() throws Exception
    {
        RestaurantDto restaurantDto = getRestaurantDtoForAdding();
        restaurantDto.setPhoneNumber("p".repeat(21));

        MvcResult result =
                mvc.perform(post("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        assertEquals(result.getResponse().getContentAsString(), "Not valid data is given.");
        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    void addNewRestaurant() throws Exception
    {
        RestaurantDto restaurantDto = getRestaurantDtoForAdding();

        MvcResult result =
                mvc.perform(post("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        RestaurantDto responseRestaurant =
                JsonUtils.mapFromJson(result.getResponse().getContentAsString(), RestaurantDto.class);

        Optional<Restaurant> restaurantFromDTB = restaurantRepository.findById(responseRestaurant.getId());

        assertFalse(restaurantFromDTB.isEmpty());

        assertEquals(201, result.getResponse().getStatus());
    }


    // Update restaurant

    @Test
    void updateExistingRestaurant() throws Exception
    {
        Optional<User> user = userRepository.getUserByEmail("AtUserR@test.com");

        assertFalse(user.isEmpty(), "User doesn't exist.");

        Optional<Restaurant> restaurant = restaurantRepository
                .findFirstByOwnerIdAndName(user.get().getId(),
                                           RestaurantControllerTest.testName);

        assertFalse(restaurant.isEmpty(),
                    "Test user hasn't restaurants, name of restaurant must be:" + RestaurantControllerTest.testName + ".");

        RestaurantDto restaurantDto = getRestaurantDtoForUpdate(restaurant.get());


        MvcResult result =
                mvc.perform(put("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        assertEquals(200, result.getResponse().getStatus());

        Optional<Restaurant> respRestaurant = restaurantRepository.findById(restaurantDto.getId());

        assertFalse(respRestaurant.isEmpty(), "Something goes wrong in data base.");

        assertEquals(restaurantDto.getName(), respRestaurant.get().getName(), "Name");

        assertEquals(restaurantDto.getAddress().getStreet(), respRestaurant.get().getAddress().getStreet(), "Street");
        assertEquals(restaurantDto.getAddress().getTown(), respRestaurant.get().getAddress().getTown(), "Town");
        assertEquals(restaurantDto.getAddress().getZipcode(),
                     respRestaurant.get().getAddress().getZipcode(),
                     "Zip code");

        assertEquals(restaurantDto.getDescription(), respRestaurant.get().getDescription(), "Description");
        assertEquals(restaurantDto.getOpenFromTime(), respRestaurant.get().getOpenFrom().toString(), "Open From");
        assertEquals(restaurantDto.getOpenTillTime(), respRestaurant.get().getOpenTill().toString(), "Open Till");
        assertEquals(restaurantDto.getPhoneNumber(), respRestaurant.get().getPhoneNumber(), "Phone Number");


        restaurantDto.setName(restaurant.get().getName());
        mvc.perform(put("/hungerkiller/v1/restaurant")
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .content(JsonUtils.mapToJson(restaurantDto))
                            .header("Authorization", "Bearer " + this.restaurateurToken))
           .andReturn();
    }

    @Test
    void updateNotExistingRestaurant() throws Exception
    {
        AddressDto addressDto = new AddressDto();
        addressDto.setStreet("Street");
        addressDto.setTown("Town");
        addressDto.setZipcode("123456");

        RestaurantDto restaurantDto = new RestaurantDto();
        restaurantDto.setId(2147483648L);
        restaurantDto.setName(RestaurantControllerTest.testName);
        restaurantDto.setAddress(addressDto);
        restaurantDto.setDescription("description");
        restaurantDto.setOpenFromTime("11:00:00");
        restaurantDto.setOpenTillTime("11:00:00");
        restaurantDto.setPhoneNumber("1234556788");


        MvcResult result =
                mvc.perform(put("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        assertEquals(404, result.getResponse().getStatus());
    }

    @Test
    void updateNotYoursRestaurant() throws Exception
    {
        Optional<User> user = userRepository.getUserByEmail("AtUserR@test.com");

        assertFalse(user.isEmpty(), "User doesn't exist.");

        Optional<Restaurant> restaurant = restaurantRepository
                .findFirstByOwnerIdAndName(user.get().getId(),
                                           RestaurantControllerTest.testName);

        assertFalse(restaurant.isEmpty(),
                    "Test user hasn't restaurants, name of restaurant must be:" + RestaurantControllerTest.testName + ".");

        RestaurantDto restaurantDto = getRestaurantDtoForUpdate(restaurant.get());


        MvcResult result =
                mvc.perform(put("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.clientToken))
                   .andReturn();

        assertEquals(403, result.getResponse().getStatus());
    }


    @Test
    void updateExistingRestaurantNotValidName() throws Exception
    {
        Optional<User> user = userRepository.getUserByEmail("AtUserR@test.com");

        assertFalse(user.isEmpty(), "User doesn't exist.");

        Optional<Restaurant> restaurant = restaurantRepository
                .findFirstByOwnerIdAndName(user.get().getId(),
                                           RestaurantControllerTest.testName);

        assertFalse(restaurant.isEmpty(),
                    "Test user hasn't restaurants, name of restaurant must be:" + RestaurantControllerTest.testName + ".");

        RestaurantDto restaurantDto = getRestaurantDtoForUpdate(restaurant.get());
        restaurantDto.setName("n".repeat(51));


        MvcResult result =
                mvc.perform(put("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    void updateExistingRestaurantValidStreet() throws Exception
    {
        Optional<User> user = userRepository.getUserByEmail("AtUserR@test.com");

        assertFalse(user.isEmpty(), "User doesn't exist.");

        Optional<Restaurant> restaurant = restaurantRepository
                .findFirstByOwnerIdAndName(user.get().getId(),
                                           RestaurantControllerTest.testName);

        assertFalse(restaurant.isEmpty(),
                    "Test user hasn't restaurants, name of restaurant must be:" + RestaurantControllerTest.testName + ".");

        RestaurantDto restaurantDto = getRestaurantDtoForUpdate(restaurant.get());
        restaurantDto.getAddress().setStreet("s".repeat(51));


        MvcResult result =
                mvc.perform(put("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    void updateExistingRestaurantNotValidTown() throws Exception
    {
        Optional<User> user = userRepository.getUserByEmail("AtUserR@test.com");

        assertFalse(user.isEmpty(), "User doesn't exist.");

        Optional<Restaurant> restaurant = restaurantRepository
                .findFirstByOwnerIdAndName(user.get().getId(),
                                           RestaurantControllerTest.testName);

        assertFalse(restaurant.isEmpty(),
                    "Test user hasn't restaurants, name of restaurant must be:" + RestaurantControllerTest.testName + ".");

        RestaurantDto restaurantDto = getRestaurantDtoForUpdate(restaurant.get());
        restaurantDto.getAddress().setTown("t".repeat(51));


        MvcResult result =
                mvc.perform(put("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    void updateExistingRestaurantNotValidZipcode() throws Exception
    {
        Optional<User> user = userRepository.getUserByEmail("AtUserR@test.com");

        assertFalse(user.isEmpty(), "User doesn't exist.");

        Optional<Restaurant> restaurant = restaurantRepository
                .findFirstByOwnerIdAndName(user.get().getId(),
                                           RestaurantControllerTest.testName);

        assertFalse(restaurant.isEmpty(),
                    "Test user hasn't restaurants, name of restaurant must be:" + RestaurantControllerTest.testName + ".");

        RestaurantDto restaurantDto = getRestaurantDtoForUpdate(restaurant.get());
        restaurantDto.getAddress().setZipcode("z".repeat(7));


        MvcResult result =
                mvc.perform(put("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    void updateExistingRestaurantNotValidDescription() throws Exception
    {
        Optional<User> user = userRepository.getUserByEmail("AtUserR@test.com");

        assertFalse(user.isEmpty(), "User doesn't exist.");

        Optional<Restaurant> restaurant = restaurantRepository
                .findFirstByOwnerIdAndName(user.get().getId(),
                                           RestaurantControllerTest.testName);

        assertFalse(restaurant.isEmpty(),
                    "Test user hasn't restaurants, name of restaurant must be:" + RestaurantControllerTest.testName + ".");

        RestaurantDto restaurantDto = getRestaurantDtoForUpdate(restaurant.get());
        restaurantDto.setDescription("d".repeat(201));


        MvcResult result =
                mvc.perform(put("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    void updateExistingRestaurantNotValidFromTime() throws Exception
    {
        Optional<User> user = userRepository.getUserByEmail("AtUserR@test.com");

        assertFalse(user.isEmpty(), "User doesn't exist.");

        Optional<Restaurant> restaurant = restaurantRepository
                .findFirstByOwnerIdAndName(user.get().getId(),
                                           RestaurantControllerTest.testName);

        assertFalse(restaurant.isEmpty(),
                    "Test user hasn't restaurants, name of restaurant must be:" + RestaurantControllerTest.testName + ".");

        RestaurantDto restaurantDto = getRestaurantDtoForUpdate(restaurant.get());
        restaurantDto.setOpenFromTime("00:11:3d");


        MvcResult result =
                mvc.perform(put("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    void updateExistingRestaurantNotValidTillTime() throws Exception
    {
        Optional<User> user = userRepository.getUserByEmail("AtUserR@test.com");

        assertFalse(user.isEmpty(), "User doesn't exist.");

        Optional<Restaurant> restaurant = restaurantRepository
                .findFirstByOwnerIdAndName(user.get().getId(),
                                           RestaurantControllerTest.testName);

        assertFalse(restaurant.isEmpty(),
                    "Test user hasn't restaurants, name of restaurant must be:" + RestaurantControllerTest.testName + ".");

        RestaurantDto restaurantDto = getRestaurantDtoForUpdate(restaurant.get());
        restaurantDto.setOpenTillTime("00:11:3d");


        MvcResult result =
                mvc.perform(put("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    void updateExistingRestaurantNotValidPhoneNumber() throws Exception
    {
        Optional<User> user = userRepository.getUserByEmail("AtUserR@test.com");

        assertFalse(user.isEmpty(), "User doesn't exist.");

        Optional<Restaurant> restaurant = restaurantRepository
                .findFirstByOwnerIdAndName(user.get().getId(),
                                           RestaurantControllerTest.testName);

        assertFalse(restaurant.isEmpty(),
                    "Test user hasn't restaurants, name of restaurant must be:" + RestaurantControllerTest.testName + ".");

        RestaurantDto restaurantDto = getRestaurantDtoForUpdate(restaurant.get());
        restaurantDto.setPhoneNumber("p".repeat(21));


        MvcResult result =
                mvc.perform(put("/hungerkiller/v1/restaurant")
                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                    .content(JsonUtils.mapToJson(restaurantDto))
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        assertEquals(400, result.getResponse().getStatus());
    }

    // Delete restaurant

    @Test
    void deleteRestaurant() throws Exception
    {
        Optional<User> user = userRepository.getUserByEmail("AtUserR@test.com");

        assertFalse(user.isEmpty(), "User doesn't exist.");

        Optional<Restaurant> restaurant = restaurantRepository
                .findFirstByOwnerIdAndName(user.get().getId(),
                                           RestaurantControllerTest.testName
                );

        assertFalse(restaurant.isEmpty(),
                    "Test user hasn't restaurants to delete, name of restaurant must be:" + RestaurantControllerTest.testName + ".");

        Optional<Dish> dish = dishRepository.findFirstByRestaurantId(restaurant.get().getId());

        MvcResult result =
                mvc.perform(delete("/hungerkiller/v1/restaurant/id/" + restaurant.get().getId().toString())
                                    .header("Authorization", "Bearer " + this.restaurateurToken))
                   .andReturn();

        assertEquals(200, result.getResponse().getStatus());

        assertTrue(restaurantRepository.findById(restaurant.get().getId()).isEmpty());
        assertTrue(addressRepository.findById(restaurant.get().getAddress().getId()).isEmpty());

        dish.ifPresent(value -> assertTrue(dishRepository.findById(value.getId()).isEmpty()));

    }

    @Test
    void deleteNotYoursRestaurant() throws Exception
    {
        Optional<User> user = userRepository.getUserByEmail("AtUserR@test.com");

        assertFalse(user.isEmpty(), "User doesn't exist.");

        Optional<Restaurant> restaurant = restaurantRepository
                .findFirstByOwnerIdAndName(user.get().getId(),
                                           RestaurantControllerTest.testName
                );

        assertFalse(restaurant.isEmpty(),
                    "Test user hasn't restaurants to delete, name of restaurant must be:" + RestaurantControllerTest.testName + ".");


        MvcResult result =
                mvc.perform(delete("/hungerkiller/v1/restaurant/id/" + restaurant.get().getId().toString())
                                    .header("Authorization", "Bearer " + this.clientToken))
                   .andReturn();

        assertEquals(403, result.getResponse().getStatus());
    }

    @Test
    void deleteRestaurantWithNotExistingId() throws Exception
    {
        Optional<User> user = userRepository.getUserByEmail("AtUserR@test.com");

        assertFalse(user.isEmpty(), "User doesn't exist.");

        Optional<Restaurant> restaurant = restaurantRepository
                .findFirstByOwnerIdAndName(user.get().getId(),
                                           RestaurantControllerTest.testName);

        assertFalse(
                restaurant.isEmpty(),
                "Test user hasn't restaurants to delete, name of restaurant must be:" + RestaurantControllerTest.testName + "."
        );


        MvcResult result = mvc.perform(delete("/hungerkiller/v1/restaurant/id/" + "2147483648")
                                               .header("Authorization", "Bearer " + this.restaurateurToken))
                              .andReturn();

        assertEquals(404, result.getResponse().getStatus());
    }

}