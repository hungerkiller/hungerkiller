package pl.umcs.HungerKiller.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class JsonUtils {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static String mapToJson(Object obj) throws JsonProcessingException
    {
        return objectMapper.writeValueAsString(obj);
    }

    public static <T> T mapFromJson(String json, Class<T> objectClass)
            throws IOException
    {
        return objectMapper.readValue(json, objectClass);
    }

    public static <T> T[] mapFromLIst(String json, Class<T[]> objectClass) throws JsonProcessingException
    {
        return objectMapper.readValue(json, objectClass);
    }
}
